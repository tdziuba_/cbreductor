var path = require('path'),
	webpack = require('webpack'),
	ExtractTextPlugin = require("extract-text-webpack-plugin"),
	CleanWebpackPlugin = require('clean-webpack-plugin'),
	sassLoaderPaths = [path.resolve(__dirname, "./scss")];

sassLoaderPaths = sassLoaderPaths.concat(require('node-neat').includePaths);

module.exports = {
	devtool: 'eval',
	debug: true,
	watch: true,
	aggregateTimeout: 300,
	poll: 50000,
	entry: {
		index: ['./src/index.js'],
		common: [ 'react', 'react-dom', 'react-router', 'react-redux', 'axios', './src/helpers', './src/reducers', 'tocca' ]
	},
	output: {
		path: path.join(__dirname, 'public'),
		filename: 'js/[name].js',
		chunkFilename: 'js/[name].chunk.js',
		publicPath: 'http://cbse-node.citiboard.se/'
	},

	sassLoader: {
		includePaths: sassLoaderPaths
	},
	plugins: [
		new webpack.optimize.OccurenceOrderPlugin(),
		// new webpack.optimize.DedupePlugin(),
		// new webpack.optimize.MinChunkSizePlugin({minChunkSize: 600}), //odkomentować dla produkcji
		new webpack.optimize.CommonsChunkPlugin({
			names: ["common"],
			minChunks: 2
		}),
		new ExtractTextPlugin("css/[name].css", {
			allChunks: true,
			disable: false
		})
	],
	module: {
		loaders: [
			{test: /\.scss$/, loader: ExtractTextPlugin.extract("style-loader", "css!sass-loader")},
			{
				test: /\.js$/,
				loaders: ['babel-loader'],
				exclude: path.join(__dirname, 'node_modules'),
				include: path.join(__dirname, 'src')
			},
			{
				test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
				loader: 'imports?define=>false&this=>window'
			},
			{
				test: /\.json$/,
				loaders: ['json'],
				exclude: path.join(__dirname, 'node_modules'),
				include: path.join(__dirname, 'src')
				// exclude: /node_modules/,
				// include: __dirname
			},

			{test: /\.woff$/, loader: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff"},
			{test: /\.ttf$/, loader: "file-loader?prefix=font/"},
			{test: /\.eot$/, loader: "file-loader?prefix=font/"},
			{test: /\.svg$/, loader: "file-loader?prefix=font/"},

			//img
			{test: /\.(png|jpg)$/, loader: 'file-loader?name=img/[name].[ext]'}
		]
	}
}

