/**
 * Created by tdziuba on 01.04.2016.
 */
var fs = require('fs'),
	path = require('path')
	_ = require('underscore');

module.exports = [
	{
		path: '/offers',
		method: 'get',
		cb: function (req, res, next) {
			var file = path.resolve(__dirname, '../jsons/offers0.json');

			if (req.query.offset) {
				file = path.resolve(__dirname, '../jsons/offers' + req.query.offset + '.json');
			}

			fs.readFile(file, 'utf8', function (err, data) {
				if (err) {
					console.warn(err);
				} else {
					var list = JSON.parse(data),
						offers = list.data.results_array;

					offers = _.filter(offers, function (item) {
						return ( item.price && parseInt(item.price) >= req.query.min && parseInt(item.price) <= req.query.max )
					});

					if (req.query.sort && req.query.sort == 'price_asc') {
						offers = _.sortBy(offers, function (item) {
							return (parseFloat(item.price))
						});
					} else if (req.query.sort && req.query.sort == 'price_desc') {
						offers = _.sortBy(offers, function (item) {
							return (parseFloat(item.price))
						}).reverse()
					} else if (req.query.sort && req.query.sort == 'newest') {
						offers = _.sortBy(offers, 'creation_date')
					}

					list.data.results_array = offers;

					res.json(list);
				}
			});
		}
	},
	{
		path: '/offer-details',
		method: 'get',
		cb: function (req, res, next) {
			var file = path.resolve(__dirname, '../jsons/offers0.json'),
				file2 = path.resolve(__dirname, '../jsons/offers60.json'),
				id = (req.query.hasOwnProperty('id')? req.query.id : req.params.id),
				json1, json2, final, offerData,
				searchById = function (json, id) {

					offerData = json.filter(function(item) {
						return (parseInt(item.id) === parseInt(id));
					});

					if (offerData.length) {
						res.json({ status: 'success', data: offerData[0] });
					} else {
						res.json({ status: 'error', message: 'offer with id: ' + id + ' is not found' });
					}

				};

			fs.readFile(file, 'utf8', function (err, data) {
				if (err) {
					console.warn(err);
				} else {
					json1 = JSON.parse(data);

					fs.readFile(file, 'utf8', function (err, data2) {
						if (err) {
							console.warn(err);
						} else {
							json2 = JSON.parse(data2);
							final = json1.data.results_array.concat(json2.data.results_array)

							searchById(final, id);
						}
					});

				}
			});
			

		}
	},
	{
		path: '/upload',
		method: 'post',
		cb: function (req, res, next) {

			res.send( JSON.stringify({
				status: 'success',
				data: [],
				message: 'All files uploaded'
			}) )
		}
	}
]