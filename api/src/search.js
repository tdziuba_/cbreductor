/**
 * Created by tdziuba on 01.04.2016.
 */
var fs = require('fs'),
	path = require('path'),
	_ = require('underscore');

module.exports = [
	{
		path: '/autocompleter',
		method: 'all',
		cb: function (req, res, next) {
			var file = path.resolve(__dirname, '../jsons/autocompleter.json');
			var q = req.query.q;

			fs.readFile(file, 'utf8', function (err, data) {
				if (err) {
					console.warn(err);
				} else {
					var d = JSON.parse(data)

					var searched = _.filter(d, function (item) {
						return (item.suggestion.indexOf(q, 0) > -1)
					})
					
					res.json({
						"status": "success",
						"code": "data_read_ok",
						"message": null,
						"data": {
							"results": searched
						}
					});
				}
			});
		}
	},

]