/**
 * Created by tdziuba on 01.04.2016.
 */
var fs = require('fs'),
	path = require('path');

module.exports = [
	{
		path: '/user/logout',
		method: 'get',
		cb: function (req, res, next) {
			var file = path.resolve(__dirname, '../jsons/user-logged-out.json');

			fs.readFile(file, 'utf8', function (err, data) {
				if (err) {
					res.json(JSON.parse( { "status": "error", "message": "problem with user logout" } ))
				} else {
					res.json(JSON.parse(data));
				}
			});
		}
	},
	{
		path: '/user/login',
		method: 'post',
		cb: function (req, res, next) {
			var file = path.resolve(__dirname, '../jsons/user-logged-in.json');

			fs.readFile(file, 'utf8', function (err, data) {
				if (err) {
					console.warn(err);
				} else {
					res.json(JSON.parse(data));
				}
			});
		}
	},
]