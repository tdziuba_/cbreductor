/**
 * Created by tdziuba on 01.04.2016.
 */

var fs = require('fs'),
	express = require('express'),
	app = new (express)(),
	router = express.Router(),
	port = 9999,
	routes = [],
	addRoutes,
	getMethod, postMethod, putMethod, deleteMethod, allMethod;

getMethod = function (routeObj) {
	router.get(routeObj.path, function (req, res, next) {

		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "X-Requested-With");

		routeObj.cb(req, res, next);

	});

	return router;
};

postMethod = function (routeObj) {
	router.post(routeObj.path, function (req, res, next) {

		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "X-Requested-With");

		routeObj.cb(req, res, next);
	});

	return router;
};

putMethod = function (routeObj) {
	router.put(routeObj.path, function (req, res, next) {

		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "X-Requested-With");

		routeObj.cb(req, res, next);
	});

	return router;
};

deleteMethod = function (routeObj) {
	router.delete(routeObj.path, function (req, res, next) {

		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "X-Requested-With");

		routeObj.cb(req, res, next);
	});

	return router;
};

allMethod = function (routeObj) {
	router.all(routeObj.path, function (req, res, next) {

		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "X-Requested-With");

		routeObj.cb(req, res, next);
	});

	return router;
};

addRoutes = function (routeData) {

	if (routeData instanceof Array) {
		for(var i in routeData) {
			routes.push(routeData[i]);
		}
	} else {
		routes.push(routeData);
	}

}

//dodajemy poszczególne trasy
addRoutes( require('./src/offers') ); //offers
addRoutes( require('./src/search') ); //autocompleter i wyniki wyszukiwania
addRoutes( require('./src/user') ); //user

console.log(routes);

var registerRoutes = function () {

	if (routes.length) {

		for (var i in routes) {
			switch (routes[i].method) {
				case 'get':
					getMethod(routes[i]);
					break;
				case 'post':
					postMethod(routes[i]);
					break;
				case 'put':
					putMethod(routes[i]);
					break;
				case 'delete':
					deleteMethod(routes[i]);
					break;
				default:
					allMethod(routes[i]);
			}
		}

	}

	return router;
}

router.get("/", function(req, res) {
	res.send('Hi i\'m api server');
})

registerRoutes();

app.use('/', router);
//
// app.get("/offers", function(req, res) {
// 	var file = '/jsons/offers.json';
//
// 	if (req.query.offset) {
// 		file = '/jsons/offers' + req.query.offset + '.json';
// 	}
//
// 	fs.readFile(__dirname + file, 'utf8', function (err, data) {
// 		if (err) {
// 			console.warn(err);
// 		} else {
// 			res.json(JSON.parse(data));
// 		}
// 	});
//
// })

app.listen(port, function(error) {
	if (error) {
		console.error(error)
	} else {
		console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser. Ctrl+C to stop", port, port)
	}
})
