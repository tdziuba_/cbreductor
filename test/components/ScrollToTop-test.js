/**
 * Created by tdziuba on 2016-05-16.
 */

import { expect } from 'chai'
import React from 'react'
import sinon from 'sinon'
import { shallow, mount, render } from 'enzyme'
import ScrollToTop from '../../src/components/ScrollToTop'
import '../.setup'


describe('ScrollToTop', function() {
	it('contains spec with an expectation', function() {
		expect(shallow(<ScrollToTop />).contains(<i className="fa fa-angle-up" />)).to.equal(true)
	})

	it('contains spec with an expectation', function() {
		expect(shallow(<ScrollToTop />).is('.goUpBtn')).to.equal(true)
	})

	it('contains spec with an expectation', function() {
		expect(render(<ScrollToTop />).find('.goUpBtn').length).to.equal(1)
	})

	it('calls componentDidMount', () => {
		sinon.spy(ScrollToTop.prototype, 'componentDidMount');
		const wrapper = mount(<ScrollToTop />);
		expect(ScrollToTop.prototype.componentDidMount.calledOnce).to.be.true;
		ScrollToTop.prototype.componentDidMount.restore();
	});

	it('calls scrollToTop method on click', function () {
		const onButtonClick = sinon.spy(ScrollToTop.prototype, 'scrollToPageTop');
		const wrapper = mount(
			<ScrollToTop onClick={onButtonClick} />
		)
		wrapper.find('button').simulate('click')
		expect(onButtonClick.calledOnce).to.equal(true)
	})
})

