var path = require('path')
var webpack = require('webpack'),
    StatsPlugin = require('stats-webpack-plugin'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    sassLoaderPaths = [path.resolve(__dirname, "./scss")];

sassLoaderPaths = sassLoaderPaths.concat(require('node-neat').includePaths);

module.exports = {
  devtool: 'eval',
  entry: [
    'webpack/hot/dev-server',
    'webpack-hot-middleware/client',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].chunk.js',
    publicPath: 'http://localhost:3000/static/'
  },

  sassLoader: {
    includePaths: sassLoaderPaths
  },
  plugins: [
    new StatsPlugin('stats.json', {
      chunkModules: true,
      exclude: [/node_modules[\\\/]react/]
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new ExtractTextPlugin("css/[name].css", {
      allChunks: true,
      disable: false
    })
  ],
  resolveLoader: {
    alias: {
      "react-proxy": path.join(__dirname, "./src")
    }
  },
  resolve: {
    extensions: ["", ".jsx", ".js", ".scss"]
  },
  module: {
    loaders: [
      { test: /\.scss$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader") },
      {
        test: /\.js$/,
        loaders: [ 'babel-loader' ],
        exclude: /(node_modules|bower_components)/,
        include: __dirname
      },
      {
        test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
        loader: 'imports?define=>false&this=>window'
      },
      {
        test: /\.json$/,
        loaders: [ 'json' ],
        exclude: /node_modules/,
        include: __dirname
      },

      { test: /\.woff$/, loader: "url-loader?prefix=font/&limit=5000&mimetype=application/font-woff" },
      { test: /\.ttf$/,  loader: "file-loader?prefix=font/" },
      { test: /\.eot$/,  loader: "file-loader?prefix=font/" },
      { test: /\.svg$/,  loader: "file-loader?prefix=font/" },

        //img
      { test: /\.(png|jpg)$/, loader: 'file-loader?name=img/[name].[ext]' }
    ]
  }
}

