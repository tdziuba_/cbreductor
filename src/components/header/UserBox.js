/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-29
 */

import React, {Component} from 'react'
import { Link } from 'react-router';
import Avatar from '../common/Avatar'
import OptionsBox from '../common/OptionsBox'

class UserBox extends Component {
	constructor(props) {
		super(props)
	}

	getAvatar(user) {
		return (
			<Avatar img={user.userAvatar} url={'/profile'} username={user.userName} wrapper={false}/>
		)
	}

	render() {
		let { user } = this.props

		return (
			<OptionsBox cssClass="avatar" content={ this.getAvatar(user) } parent={ this } >
				<li><Link to="/"><i className="icon icon-home" aria-hidden="true" /><span>Discover</span></Link></li>
				<li><Link to={`/${ user.isLoggedIn ? 'add-offer' : 'access' }`}><i className="icon icon-camera" aria-hidden="true" /><span>Sell your stuff</span></Link></li>
				<li><i className="icon icon-chat" aria-hidden="true" /><span>Chat</span></li>
				<li><Link to="/profile"><i className="icon icon-user" aria-hidden="true" /><span>My Profile</span></Link></li>
				<li><i className="icon icon-envelope" aria-hidden="true" /><span>Contact us</span></li>
				<li><i className="icon icon-help" aria-hidden="true" /><span>Help</span></li>
				<li className="separated">
					<Link to={`/access/${ user.isLoggedIn ? 'logout' : 'login' }`}><i className={`icon icon-${ user.isLoggedIn ? 'logout' : 'login' }`} aria-hidden="true"/><span>{ user.isLoggedIn ? 'logout' : 'login' }</span></Link>
				</li>
			</OptionsBox>
		)

	}
}

export default UserBox