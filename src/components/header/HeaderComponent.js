/**
 * Created by tdziuba on 01.04.2016.
 */

import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import NavBar from './NavBar';
import SearchBar from '../../containers/common/SearchBarContainer.js';
import AddOfferBtn from './AddOfferBtn';
import UserBox from './UserBox';
import LastActivityLayer from '../../containers/common/LastActivityLayer'
import SideMenu from './SideMenu'
import { addEvent, removeEvent } from '../../helpers'

if (process.browser) {
	require('./scss/header.scss');
}

connect(state => ({ state: state, routerState: state.router}))
class Header extends Component{
	constructor(props){
		super(props);
	}

	componentDidMount() {
		addEvent( this.refs.menuBtn, 'click', this.showMenu.bind(this) )

	}

	componentWillUnmount() {
		removeEvent( this.refs.menuBtn, 'click', this.showMenu )
		removeEvent( this.refs.searchExpandBtn, 'click', this.showSearchForm )
	}

	showMenu(ev) {
		this.refs.sideMenu.show()
	}

	hideMenu() {
		this.hide()
	}
	
	render() {

		let { state } = this.props

		return (
			<header className="appHeader">
				<div className="container">
					<div className="row vmid appHeaderActions">
						<div className="col logoWrap">
							<span className="btn menu-btn" ref="menuBtn"><i className="icon icon-mobile-menu" aria-hidden="true"/></span>
							<h1><Link to="/" className="logo text-out">Citiboard.se</Link></h1>
						</div>
						<SearchBar ref="searchBar" />
						<div className="col userActionsWrp">
							<div className="row appHeaderButtons">
								<LastActivityLayer />
								<UserBox user={state.user} />
								<AddOfferBtn user={state.user} />
							</div>
						</div>
						<SideMenu ref="sideMenu" parent={this} user={state.user} />
					</div>
				</div>
			</header>
		)
	}

};

export default Header;