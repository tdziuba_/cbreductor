/**
 * Created by tdziuba on 31.03.2016.
 */
import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import _ from 'underscore'
import { addEvent, removeEvent } from '../../helpers'

import Autocompleter from './Autocompleter'

if (process.browser) {
	require('./scss/_searchbar.scss');
}

class SearchBar extends Component{
	constructor(props){
		super(props);
		this.parent = this.props.parent
		this.autocompleter = null
		this.searchForm = null


		this.searchField = null
		this.isFetching = false
		this.autocompleterResults = null
		this.phrase = ''
		this.showForm = false
	}

	//nasłuchujemy eventów związanych z formularzem wyszukiwania
	componentDidMount() {
		let self = this

		addEvent( self.refs.searchField, 'input', _.debounce(self.onTextEnter.bind(this), 350, false))
		addEvent( this.refs.searchExpandBtn, 'click', this.showSearchForm.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.searchField, 'input', this.onTextEnter )
	}

	componentDidUpdate() {

	}

	showSearchForm() {
		this.showForm = (!this.showForm)

		this.forceUpdate()
	}

	onTextEnter(ev) {
		this.phrase = ev.target.value

		if (this.phrase.length > 1) {
			this.parent.fetchAutocompleter(this.phrase)
		}
	}

	showAutocompleter(results) {
		this.autocompleter.setProps(results, this.phrase)

		this.autocompleter.forceUpdate()
	}

	render() {

		return (
			<div className="col searchBarWrp">
				<span className="btn search-expand-btn" ref="searchExpandBtn"><i className="icon icon-search" aria-hidden="true"/></span>
				<form className={`searchForm form inline-form ${ this.showForm ? 'open' : '' }`} ref={ (ref) => this.searchForm = ref }>
					<div className="formRow grouped">
						<input type="search" id="searchInput" className="input input-large searchInput col-14of16" placeholder="What are you looking for?" ref="searchField" />
						<button className="btn btn-medium col-2of16 searchBtn"><span>Search</span></button>
						<div className="col-14of14 autocompleterWrp">
							<Autocompleter suggestions={this.autocompleterResults} phrase={this.phrase} ref={(ref) => this.autocompleter = ref} />
						</div>
					</div>
				</form>
			</div>
		)
	}
};

export default SearchBar;