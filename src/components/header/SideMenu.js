/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 24.05.2016  14:09
 */

import React, { Component } from 'react'
import { Link } from 'react-router'
import { addEvent, removeEvent } from '../../helpers'
import Avatar from '../common/Avatar'

if (process.browser) {
	require('tocca')
}


class SideMenu extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
		this.showMenu = false;
	}
	
	componentDidMount() {
		addEvent( this.refs.closeBtn, 'click', this.parent.hideMenu.bind(this) )
		addEvent( this.refs.sideMenuList, 'click', this.parent.hideMenu.bind(this) )
		addEvent( this.refs.userBox, 'click', this.parent.hideMenu.bind(this) )
		addEvent( this.refs.getAppBtn, 'click', this.parent.hideMenu.bind(this) )
		addEvent( this.refs.closeBtn, 'click', this.parent.hideMenu.bind(this) )
		addEvent( this.refs.sideMenu, 'swipeleft', this.parent.hideMenu.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.closeBtn, 'click', this.parent.hideMenu )
		removeEvent( this.refs.sideMenuList, 'click', this.parent.hideMenu )
		removeEvent( this.refs.userBox, 'click', this.parent.hideMenu )
		removeEvent( this.refs.getAppBtn, 'click', this.parent.hideMenu )
		removeEvent( this.refs.sideMenu, 'swipeleft', this.parent.hideMenu )
	}

	show() {
		this.showMenu = true
		this.forceUpdate()
	}

	hide() {
		this.showMenu = false
		this.forceUpdate()
	}

	render() {
		let { user }  = this.props
		let showMenu = this.showMenu
		let url = (user.isLoggedIn) ? '/profile' : '/access'

		return (
			<div className={`sideMenu ${ showMenu ? 'open' : 'close' }`} ref="sideMenu">
				<span className="btn closeBtn" role="button" ref="closeBtn"><i className="icon icon-circle-x" aria-hidden="true" /></span>
				<div className="userBox" ref="userBox">
					<Avatar user={user} img={ user.userAvatar } url={url} wrapper={true}/>
					<h3 className="userName"><Link to={url}>{ (user.username) ? user.username : 'Log in or create free account' }</Link></h3>
				</div>
				<ul className="sideMenuList" ref="sideMenuList">
					<li><Link to="/"><i className="icon icon-home" aria-hidden="true" /><span>Discover</span></Link></li>
					<li><Link to={`/${ user.isLoggedIn ? 'add-offer' : 'access' }`}><i className="icon icon-camera" aria-hidden="true" /><span>Sell your stuff</span></Link></li>
					<li><Link to="/messages"><i className="icon icon-chat" aria-hidden="true" /><span>Chat</span></Link></li>
					<li><Link to={`/${ user.isLoggedIn ? 'profile' : 'access' }`}><i className="icon icon-user" aria-hidden="true" /><span>My Profile</span></Link></li>
					<li><Link to="/contact"><i className="icon icon-envelope" aria-hidden="true" /><span>Contact us</span></Link></li>
					<li><Link to="/help"><i className="icon icon-help" aria-hidden="true" /><span>Help</span></Link></li>
					<li>
						<Link to={`/access/${ user.isLoggedIn ? 'logout' : 'login' }`}><i className={`icon icon-${ user.isLoggedIn ? 'logout' : 'login' }`} aria-hidden="true"/><span>{ user.isLoggedIn ? 'logout' : 'login' }</span></Link>
					</li>
				</ul>
				<a href="" className="btn btn-secondary btn-large getAppBtn" ref="getAppBtn" target="_blank">Get free app!</a>
			</div>
		)
	}
}

export default SideMenu
