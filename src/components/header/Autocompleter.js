/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-25
 */

import React, {Component} from 'react'
import $ from 'jquery'
import { addEvent, removeEvent } from '../../helpers'

class Autocompleter extends Component {
	constructor(props) {
		super(props)
		this.suggestions = []
		this.phrase = ''
		this.suggestionList = null
		this.layer = null
		this.isHidden = true
	}

	setProps(suggestions, phrase) {
		let self = this
		this.suggestions = suggestions
		this.phrase = phrase
		this.isHidden = false
	}

	componentDidMount() {
		let self = this

		addEvent(self.layer, 'click', self.hideAutocompleter.bind(this))
	}

	componentWillUnmount() {
		removeEvent(self.layer, 'click', self.hideAutocompleter.bind(this))
	}

	hideAutocompleter() {
		this.suggestions = []
		this.phrase = ''
		this.isHidden = true
		this.forceUpdate()
	}

	render() {
		let self = this
		let items = (this.suggestions) ? this.suggestions.map(function (item) {

			let bolded = (<span><strong>{ item.suggestion.substring(0, self.phrase.length) }</strong>{ item.suggestion.substring(self.phrase.length+1, item.suggestion.length) }</span> )
			return (
				<a key={item.suggestion} className="autocompleterSuggestion" href="">{ bolded }</a>
			)
		}) : null

		return (
			<div className={`autocompleter${this.isHidden ? ' hidden': ''}`} ref={(ref) => this.suggestionList = ref}>
				<div className="autocompleterList">{ items }</div>
				<div className="autocompleterLayer" ref={(ref) => this.layer = ref}/>
			</div>
		)
	}
}

export default Autocompleter