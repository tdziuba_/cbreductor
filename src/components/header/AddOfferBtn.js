/**
 * Created by tdziuba on 31.03.2016.
 */
import React, { Component } from 'react'
import { Link } from 'react-router'


class AddOfferBtn extends Component {
	constructor(props) {
		super(props)
		this.offerPopup = null
	}

	render() {
		let { user } = this.props

		return (
			<Link className="btn btn-large btn-secondary addOfferBtn" to={ user.isLoggedIn ? '/add-offer' : '/access' }>Sell your stuff <i className="icon icon-camera" aria-hidden="true"/></Link>
		)
	}
}

export default AddOfferBtn
