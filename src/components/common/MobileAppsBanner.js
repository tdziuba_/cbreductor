/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-02
 */

import React, {Component} from 'react'
import Storage, { addEvent, removeEvent } from '../../helpers'

if (process.browser) {
	require('./mobile-apps-banner.scss')
}

class MobileAppsBanner extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		addEvent( this.refs.bannerBtn, 'click', this.handleBannerVisibility.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.bannerBtn, 'click', this.handleBannerVisibility )
	}

	handleBannerVisibility(ev) {
		let hideBanner = Storage.get('hideTopBanner')
		let btnClass = ev.target.className

		//btnClass = (btnClass.indexOf('hideBanner')) ? btnClass +
	}

	render() {

		return (
			<div className="topPlacement opxBox container">
				<div className="mobileAppsContainer">
					<button className="btn bannerBtn hideBanner" ref="bannerBtn"><span>Hide</span><i className="icon icon-angle-up" aria-hidden="true" /></button>
					<h3 className="slogan">Ladda ner våra appar gratis!</h3>
					<div className="buttons">
						<a href="#" className="appStoreBtn">Håmta i App Store</a>
						<a href="#" className="googlePlayBtn">På Google play</a>
					</div>
				</div>
			</div>
		)
	}
}

export default MobileAppsBanner
