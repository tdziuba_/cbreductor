/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 25.05.2016  07:39
 */

import React, { Component } from 'react'
import { Link } from 'react-router'


class Avatar extends Component {
	constructor(props) {
		super(props)
	}

	getImage() {
		const { img, username } = this.props

		return (
			<img className="avatarImg" src={img} alt={username} />
		)
	}

	getLink(cssClass) {
		const { img, url, username } = this.props

		return(
			<Link className={`avatarLink ${cssClass}`} to={url}>
				{ this.getImage() }
			</Link>
		)
	}

	getWrapper(cssClass) {
		const { url } = this.props

		if (typeof url !== 'undefined' && url) {
			return (
				<div className={`avatar ${cssClass}`}>
					{ this.getLink('') }
				</div>
			)
		} else {
			return (
				<div className="avatar">
					{ this.getImage() }
				</div>
			)
		}
	}

	render() {
		let { img, url, username, cssClass, wrapper } = this.props

		if (typeof cssClass == 'undefined') {
			cssClass = ''
		}

		return (typeof wrapper !== 'undefined' && wrapper) ? this.getWrapper(cssClass) : this.getLink(cssClass)

	}
}

export default Avatar
