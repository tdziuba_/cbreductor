/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 25.05.2016  08:52
 */

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { addEvent, removeEvent } from '../../helpers'


class OptionsBox extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
		this.showOptions = false
		this.additionaClass = (typeof this.props.cssClass !== 'undefined') ? this.props.cssClass : ''
	}

	componentDidMount() {
		addEvent( this.refs.expandBtn, 'click', this.handleClick.bind(this) )
		addEvent( this.refs.options, 'click', this.handleBlur.bind(this) )
		addEvent( this.refs.layer, 'click', this.handleBlur.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.expandBtn, 'click', this.handleClick )
		removeEvent( this.refs.options.querySelector('a'), 'click', this.handleBlur )
		removeEvent( this.refs.layer, 'click', this.hide )
	}

	handleClick(ev) {
		ev.preventDefault()

		this.showOptions = (this.showOptions)? false : true;
		this.forceUpdate()
	}

	handleBlur(ev) {
		this.showOptions = false;
		this.forceUpdate()
	}

	getButton() {
		const { content } = this.props

		 if (typeof content !== 'undefined') {
			 return (
				 <button className="btn-text expandOptions" ref="expandBtn">
					 { content }
					 <span className="options-box-arrow"><i className="icon icon-angle-down" aria-hidden="true" /></span>
				 </button>
			 )
		 } else {
			 return (
				 <button className="btn-text expandOptions" ref="expandBtn">
					 <span className="options-box-arrow"><i className="icon icon-angle-down" aria-hidden="true" /></span>
				 </button>
			 )
		 }
	}

	render() {
		let { children, cssclass } = this.props
		let openClass = (this.showOptions) ? ' open' : ''

		return (
			<div className={`options-box ${this.additionaClass} ${openClass}`} ref="box">
				{ this.getButton() }
				<ul className="options" ref="options">
					{ children }
				</ul>
				<div className="layer" ref="layer" />
			</div>
		)
	}
}

export default OptionsBox
