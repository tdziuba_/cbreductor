/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-10
 */

import React, {Component} from 'react'

class FakeRadio extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { value, label, isChecked, cssClass } = this.props

		return (
			<label className={`fakeRadioLabel ${ cssClass ? cssClass : '' }` }>
				<input type="radio" name="usage-state" className="radio hidden" value={ value } defaultChecked={ isChecked ? 'checked' : '' } />
				<span className="fakeRadio" />
				<span className="radioLabel">{ label }</span>
			</label>
		)
	}
}

export default FakeRadio