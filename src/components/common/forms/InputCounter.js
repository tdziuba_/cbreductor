/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-10
 */

import React, {Component} from 'react'
import { addEvent, removeEvent } from '../../../helpers/component-helpers'

class InputCounter extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		let self = this

		addEvent(self.refs.input, 'keyup', self.handleTyping.bind(this))
	}

	componentWillUnMount() {
		let self = this

		removeEvent(self.refs.input, 'keydown', self.handleTyping)
	}

	handleTyping(ev) {
		const { max } = this.props
		let currentLength = this.refs.input.value.length

		this.refs.currentChars.innerHTML = currentLength

		if (currentLength >= max && !ev.ctrlKey) {
			this.refs.input.value = this.refs.input.value.substring(0, max)
		}
	}

	render() {
		const { max, children } = this.props

		return (
			<div className="counterField">
				{ React.cloneElement( children, { ref: 'input' } ) }
				<span className="counter">
					<strong ref="currentChars">0</strong>
					<span>/</span>
					<strong>{ max }</strong>
				</span>
			</div>
		)
	}
}

export default InputCounter