/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-10
 */

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { addEvent, removeEvent } from '../../../helpers/component-helpers'

class FakeSelect extends Component {
	constructor(props) {
		super(props)
		this.selected = null
	}

	componentDidMount() {
		let self = this

		this.selectNode = ReactDOM.findDOMNode( self.refs.select )
		this.listNode = ReactDOM.findDOMNode( self.refs.list )

		addEvent(self.refs.select, 'click', self.handleSelectClick.bind(this))
		addEvent(self.refs.layer, 'click', self.handleClose.bind(this))
		addEvent(self.refs.list, 'click', self.handleListSelection.bind(this))
	}

	componentWillUnMount() {
		let self = this

		removeEvent(self.refs.select, 'click', self.handleSelectClick)
		removeEvent(self.refs.layer, 'click', self.handleClose)
		removeEvent(self.refs.list, 'click', self.handleListSelection)
	}

	handleSelectClick() {
		this.refs.fakeSelect.className = this.refs.fakeSelect.className + ' open'
	}

	handleClose() {
		this.refs.fakeSelect.className = this.refs.fakeSelect.className.replace(' open', '')
	}

	handleListSelection(ev) {
		let data = ev.target.dataset

		this.refs.hiddenInput.value = data.value
		this.refs.selectedText.innerHTML = data.text

		this.handleClose()
	}

	getList() {
		let { children } = this.props,
			counter = 0,
			list = [],
			child

		if (typeof children !== 'undefined' && children.length) {
			for (child of children) {
				list.push(<li key={counter} ref={`fakeSelectItem-${counter}`}>{child}</li>)
				counter++
			}

			return list
		} else {
			return ''
		}

	}

	render() {
		const { firstLabel, inputName } = this.props

		return (
			<div className="fakeSelect" ref="fakeSelect">
				<input type="hidden" value="" name={ inputName } ref="hiddenInput" />
				<div className="select select-medium" ref="select"><span ref="selectedText">{ firstLabel }</span><span className="fakeSelectArrow"><i className="fa fa-angle-down" aria-hidden="true" /></span></div>
				<ul className="fakeSelectList" ref="list">{ this.getList() }</ul>
				<div className="layer" ref="layer" />
			</div>
		)
	}
}

export default FakeSelect