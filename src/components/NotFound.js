/**
 * Created by tdziuba on 01.04.2016.
 */

import React, {Component} from 'react';

class NotFound extends Component{
	constructor(props){
		super(props);
	}

	render() {
		return (
			<div className='container'>
				<h1>404 Error - Not Found</h1>
				<p>The page you're looking for is not found</p>
			</div>
		)
	}
};

export default NotFound;