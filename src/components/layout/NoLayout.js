/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-19
 */

import React, { Component } from 'react'
//import OverlayWindow from '../popups/OverlayWindow'


class NoLayout extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const { children } = this.props

		return (
			<div className="cbApp" parent={this.parent}>
				{React.cloneElement(children, {...this.props})}
				{/*<div className="cbPopups"><OverlayWindow parent={this.parent} ref={(ref) => this.parent.overlayWindow = ref} /></div>*/}
			</div>
		)
	}
}

export default NoLayout