/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-19
 */

import React, { Component } from 'react'

import Header from '../header/HeaderComponent'
import Footer from '../footer/FooterComponent'
import ScrollToTop from '../ScrollToTop'
import InfoDialog from '../popups/InfoDialog'

class FullLayout extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
	}

	render() {
		const { children, parent, state, dispatch } = this.props

		return (
			<div className="cbApp row">
				{/* ( typeof Modernizr !== 'undefined' && Modernizr.flexbox ) ? '': <OldBrowsersInfo /> */}
				<Header state={state}/>
				<div className="cbContent container">
					{ React.cloneElement( children, { ...this.props } ) }
					<ScrollToTop />
					{ (state.errorMessage && state.errorMessage !== null) ? <InfoDialog cssClass="error" dispatch={dispatch} state={state} type="error"><h3>Error occures!</h3><p>{ state.errorMessage }</p></InfoDialog> : ''}
				</div>
				<Footer/>

				{/*<div className="cbPopups"><OverlayWindow parent={this.parent} ref={(ref) => this.parent.overlayWindow = ref} /></div>*/}
				{/*<div className="container vmid">
					<div className="row">
						<div className="col col-1of16">1of16</div>
						<div className="col col-15of16">15of16</div>
					</div>
					<div className="row">
						<div className="col col-2of16">2of16</div>
						<div className="col col-14of16">14of16</div>
					</div>
					<div className="row">
						<div className="col col-3of16">3of16</div>
						<div className="col col-13of16">13of16</div>
					</div>
					<div className="row">
						<div className="col col-4of16">4of16</div>
						<div className="col col-12of16">12of16</div>
					</div>
					<div className="row">
						<div className="col col-5of16">5of16</div>
						<div className="col col-11of16">11of16</div>
					</div>
					<div className="row">
						<div className="col col-6of16">6of16</div>
						<div className="col col-10of16">10of16</div>
					</div>
					<div className="row">
						<div className="col col-7of16">7of16</div>
						<div className="col col-9of16">9of16</div>
					</div>
					<div className="row">
						<div className="col col-8of16">8of16</div>
						<div className="col col-8of16">8of16</div>
					</div>
				</div>*/}

			</div>
		)
	}
}

export default FullLayout