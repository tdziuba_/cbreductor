/**
 * Created by tdziuba on 31.03.2016.
 */
import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import _ from 'underscore'

let Masonry = (typeof window !== 'undefined') ? require('masonry-layout') : null;
let imagesLoaded = (typeof window !== 'undefined') ? require('imagesloaded') : null;

//import Masonry from 'react-masonry-component'

const masonryOptions = {
	gridSelector: '.layout-masonry',
	itemSelector: '.gridItem',
	gutter: 0,
	percentPosition: true,
	transitionDuration: '0.2s'
};

class OfferList extends Component {

	constructor(props) {
		super(props)
		this.masonryList = null
		this.masonry = null
		this.oldChildren = []
		this.children = []
		this.grid = null
	}

	componentDidMount() {
		//this.masonry = new Masonry( '.layout-masonry', masonryOptions)

	}

	componentDidUpdate() {
		let { children } = this.props
		let self = this

		if ( children.length ) {
			this.grid = ReactDOM.findDOMNode(this.masonryList)
			if (this.grid.querySelector('.gridItem')) {
				this.handleImagesLoading()
			}

			this.children = children
			this.newChildren = _.difference(this.children, this.oldChildren)

			//this.appendNewChildrenToMasonry()
		}
	}

	getChildrenNodes() {
		const { children } = this.props
		let masonryNode = ReactDOM.findDOMNode(this.masonryList)
		let nodes = []

		_.each(children[0], function (child) {
			let node = masonryNode.querySelector('#boardItem-'+child.key)
			nodes.push(node)
		})

		return nodes
	}

	handleImagesLoading() {

		if (imagesLoaded) {

			imagesLoaded(
				'#OfferList',
				function (instance) {
					this.masonry = new Masonry( '.layout-masonry', masonryOptions)
				}.bind(this)
			);

		}

	}

// 	calculateLayout() {
// 		let children = this.getChildrenNodes()
// 		let maxW = this.grid.clientWidth
// 		let item = this.grid.querySelector('.gridItem')
// 		let gridItemW = this.grid.querySelector('.gridItem').clientWidth
// 		let itemsInRow = Math.floor(maxW/gridItemW)
// 		let nextRow = null
//
// alert('calculateLayout');
// 		let rows = _.groupBy(children, function(element, index) {
// 			return Math.floor(index / itemsInRow);
// 		});
// 		rows = _.toArray(rows);
//
// 		for (var i in rows) {
// 			var maxH = 0
// 			var tmp = []
// 			var z = (parseInt(i) + 1)
//
// 			nextRow = (typeof rows[z] !== 'undefined') ? rows[z] : null
//
// 			//sprawdzanie czy jakiś element w rzędzie jest większy
// 			for (var n = 0; n < itemsInRow; n++) {
// 				if (typeof rows[i][n] !== 'undefined') {
// 					tmp.push(rows[i][n].clientHeight)
// 				}
// 			}
// 			maxH = _.max(tmp, function(h){ return h });
//
// 			this.calculateSingleRow(itemsInRow, rows[i], nextRow, maxH)
//
// 		}
//
// 	}
//
// 	calculateSingleRow(itemsInRow, row, nextRow, maxH) {
// 		for (var n = 0; n < itemsInRow; n++) {
// 			var item = row[n],
// 				bottomNode = (nextRow && typeof nextRow[n] !== 'undefined') ? nextRow[n] : null;
//
// 			this.calculateForSingleItem(itemsInRow, row, nextRow, maxH, item, bottomNode)
// 		}
// 	}
//
// 	calculateForSingleItem(itemsInRow, row, nextRow, maxH, item, bottomNode) {
// 		//console.log('item', item)
// 		//console.log('bottomNode', bottomNode)
// 		var elemMarginTop = parseInt(window.getComputedStyle(item, null).getPropertyValue("margin-top"))
//
// 		if ((item.clientHeight + elemMarginTop < maxH) && bottomNode) {
// 			console.log(item, ' ma dziurę');
// 			bottomNode.style.cssText = bottomNode.style.cssText + 'margin-top: ' + parseInt( Math.abs( maxH - (item.clientHeight)) * -1)  + 'px;'
// 		}
// 	}
//
// 	appendNewChildrenToMasonry() {
// 		let self = this
// 		let masonryNode = ReactDOM.findDOMNode(this.masonryList)
// 		let nodes = []
//
// 		_.each(this.newChildren[0], function (child) {
// 			let node = masonryNode.querySelector('#boardItem-'+child.key)
// 			nodes.push(node)
// 			self.rebuildOnImageLoad(node)
//
// 		})
// 		this.masonry.addItems( nodes )
// 		this.masonry.layout()
// 	}
//
// 	/**
// 	 * przebudowuje layout podczas ładowania obrazków
// 	 * @param node - element DOM.gridItem
// 	 */
// 	rebuildOnImageLoad(node) {
// 		let self = this
// 		let img = new Image()
// 		let nodeImg = node.querySelector('.offerPhoto')
//
// 		img.onload = function () {
// 			//self.masonry.appended(node)
// 			self.masonry.layout()
// 		}
// 		img.src = nodeImg.src
//
//
// 	}

	render() {
		const { children } = this.props

		return (
			<div id="OfferList" className="col col-12of16 OfferList gridList">
				<div className={masonryOptions.gridSelector.replace('.', '')} ref={ (ref) => this.masonryList = this }>
					{ children }
				</div>
			</div>
		)
	}
	// render() {
	// 	const { children } = this.props
	//
	// 	return (
	// 		<div className='OfferList gridList'>
	// 			// <Masonry
	// 			// 	className={'masonry-layout'} // default ''
	// 			// 	elementType={'div'}
	// 			// 	options={masonryOptions}
	// 			// 	ref={(ref) => this.masonry = ref}
	// 			// >
	// 			// { children }
	// 			// </Masonry>
	// 		</div>
	// 	)
	// }
}
export default OfferList
