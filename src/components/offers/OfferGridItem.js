/**
 * Created by tdziuba on 07.04.2016.
 */
import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import { Link } from 'react-router'
import { urlFriendlyString, removeDiacritics, formatPrice } from '../../helpers'

if (process.browser) {
	require('./_grid-item.scss')
}

class OfferGridItem extends Component {
	constructor (props) {
		super(props)
		this.node = null
		this.setOfferLink();
	}

	setOfferLink() {
		const { title, location_info, id } = this.props.data;

		var loc = location_info.split(',').reverse(),
			titleConverted = this.getPreparedTitle(),
			location = removeDiacritics( loc[0].trim() );

		this.offerUrl = `/${location}/${titleConverted},${id}`;
	}

	getOfferLink() {
		return this.offerUrl;
	}

	getPreparedTitle() {
		const { title } = this.props.data;

		return urlFriendlyString(title);
	}

	renderComments() {
		if (this.props.comments_nb) {
			return (
				<span className="gridComments">
					<span className="commentIcon"></span> {this.props.comments_nb}
				</span>
			)
		} else {
			return (
				<span className="gridComments"/>
			)
		}
	}

	shouldComponentUpdate() {
		return false;
	}

	componentDidMount() {
		this.node = ReactDOM.findDOMNode(this)
	}

	addClassname(cssClass) {
		this.node.className = this.node.className.trim()  + ' ' + cssClass
	}

	removeClassname(cssClass) {
		this.node.className = this.node.className.trim().replace(' ' + cssClass, '')
	}

	render() {
		const o = this.props.data;

		return (
			<article itemScope="" itemType="http://schema.org/IndividualProduct" className="cntListView js_board_item gridItem" id={`boardItem-${o.id}`}>
				<meta itemProp="name" content={o.title} />
				<div itemProp="offers" itemScope="" itemType="http://schema.org/Offer" className="main clearfix">
					<div className={`picture ${ !o.pictures_count ? 'noPicture': ''}`}>
						<figure>
							<Link to={this.getOfferLink()}>
								<img className="offerPhoto" itemProp="image" src={o.picture.full ? o.picture.full : '/img/responsive/no-picture.png'} alt="Klänning från Nelly stl. S 36-38 (S) Malmö" />
							</Link>
						</figure>
						<div className="gridTitle">
							<h3>
								<Link className="gridTitleLink fade" to={this.getOfferLink()}>
									<span className="title">{o.title}</span>
								</Link>
							</h3>
						</div>
						<p className="gridPrice">{ formatPrice(o.price) }</p>
					</div>
				</div>
			</article>
		)
	}

	// render() {
	// 	const o = this.props.data;
	//
	// 	return (
	// 		<article itemScope="" itemType="http://schema.org/IndividualProduct" className="cntListView js_board_item gridItem" id={`boardItem-${o.id}`}>
	// 			<meta itemProp="name" content={o.title} />
	// 				<div itemProp="offers" itemScope="" itemType="http://schema.org/Offer" className="main clearfix">
	// 					<div className={`picture ${ !o.pictures_count ? 'noPicture': ''}`}>
	// 						<figure>
	// 							<Link to={this.getOfferLink()}>
	// 								<img className="offerPhoto" itemProp="image" src={o.picture.full ? o.picture.full : '/img/responsive/no-picture.png'} alt="Klänning från Nelly stl. S 36-38 (S) Malmö" />
	// 							</Link>
	// 						</figure>
	// 					</div>
	//
	// 					<div className="gridTitle">
	// 						<h3>
	// 							<Link className="gridTitleLink fade" to={this.getOfferLink()}>
	// 								<span className="title">{o.title}</span>
	// 							</Link>
	// 						</h3>
	// 					</div>
	// 					<p className="gridPrice">{o.price}&nbsp;kr</p>
	// 					<div className="gridFooter">
	// 						<span className="gridLocation"><span className="gridLocationName">{o.location_info}</span></span>
	// 						{ this.renderComments() }
	// 					</div>
	//
	//
	//
	// 				</div>
	//
	// 		</article>
	// 	)
	// }
}

export default OfferGridItem;