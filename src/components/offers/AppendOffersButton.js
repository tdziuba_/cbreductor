/**
 * Created by tdziuba on 08.04.2016.
 */
import React, {Component} from 'react'
import { ReactDOM } from 'react-dom'

class AppendOffersButton extends Component {
	constructor (props) {
		super(props);
	}

	render() {
		const { isFetching, parent } = this.props

		return (
			<button className={`btn btn-primary btn-large appendOffersButton ${ isFetching ? 'hidden' : ''}` } onClick={() => parent.appendMore() } >
				<strong><i className="fa fa-plus" aria-hidden="true" /> Visa mer annonser</strong>
			</button>
		)
	}
}

export default AppendOffersButton
