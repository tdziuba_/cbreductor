/**
 * Created by tdziuba on 31.03.2016.
 */
import React, {Component} from 'react';

//UWAGA! przed kompilacją należy raz ustawić zmienną SET NODE_ENV.browser=1 (windows) lub export NODE_ENV.browser=1
if (process.browser) {
	require('./spinner.scss')
}

class Spinner extends Component{
	constructor(props){
		super(props);
	}

	render() {
		return (
			<div className='spinner'>
				<div className="loading">
					<p>Loading&hellip;</p>
				</div>
			</div>
		)
	}
};

export default Spinner;