/**
 * Created by tdziuba on 14.04.2016.
 */
import React, { Component } from 'react'
import ReactDom from 'react-dom'

// if (process.browser) {
// 	require('./overlay-window.scss')
// }

class OverlayWindow extends Component {
	constructor(props) {
		super(props)
		this.isVisible = false;
		this.content = this.props.content
		this.title = ''
		this.cssClasses = ' transparent'
		this.parent = this.props.parent
		this.overlayWrapper = null
	}

	setContent(content) {
		this.content = content
	}

	setClasses( cssClasses ) {
		this.cssClasses += cssClasses
	}

	show(ev) {
		this.isVisible = true;
		let overlay = ReactDom.findDOMNode(this.overlayWrapper)
		overlay.className = overlay.className.replace(/transparent/g, '') + ' overlay-show'

		this.forceUpdate()
	}

	close(ev) {
		this.isVisible = false;
		let overlay = ReactDom.findDOMNode(this.overlayWrapper)
		overlay.className = overlay.className.replace(/overlay-show/g, '') + ' transparent'

		this.forceUpdate()
	}
	
	render() {

		return (

				<div className="overlayWindowLayer overlay transparent" ref={(ref) => this.overlayWrapper = this} >
					<div className="overlayWindowScreening" onClick={ () => this.close() } />
					<div className="overlayWindowWrapper">
						<div className="overlayWindowContent">
							<header className="overlayWindowHeader">
								{this.title ? <h3>this.title</h3> : ''}
								<a className="closeBtn" onClick={ () => this.close() }><i className="fa fa-close" aria-transparent="true"/></a>
							</header>
							<p>{ this.content ? this.content : 'Tu będzie content' }</p>
						</div>
					</div>
				</div>

		)

	}
}

export default OverlayWindow