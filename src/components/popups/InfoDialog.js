/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-04
 */

import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import { addEvent, removeEvent } from '../../helpers/component-helpers'
import { resetErrorMessage } from '../../actions/common-actions'

class InfoDialog extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		addEvent(this.refs.closeBtn, 'click', this.hideInfo.bind(this))
	}

	componentWillUnmount() {
		removeEvent(this.refs.closeBtn, 'click', this.hideInfo)
	}

	hideInfo() {
		let self = this,
			{ dispatch, state } = this.props,
			parent = this.props.parent,
			dialog = ReactDOM.findDOMNode(this)

		dialog.className = dialog.className + ' hideIt'

		if ( this.props.hasOwnProperty('type') && this.props.type === 'error' ) {
			dispatch(resetErrorMessage(dispatch, state)) //restin error
		}

		if (parent && typeof parent.unmountInfo === 'function') {
			setTimeout(function () {
				parent.unmountInfo(dialog)
			}, 300)
		}
	}

	render() {
		let { children, cssClass } = this.props

		if (typeof cssClass == 'undefined') {
			cssClass = ''
		}

		return (
			<div className={`info ${cssClass}` } ref="info">
				<button className="closeBtn" ref="closeBtn">
					<i className="fa fa-close" aria-hidden="true"><span className="hide-text">Close</span></i>
				</button>
				{ children }
			</div>
		)
	}
}

export default InfoDialog