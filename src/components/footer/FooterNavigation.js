/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-10
 */

import React, { Component } from 'react'
import { Link } from 'react-router'

class FooterNavigation extends Component {
    constructor(props) {
        super(props)
    }
    
    render() {
        const { children } = this.props
        
        return (
            <div className="footerNavigation">
                <h2 className="logo"><Link to="/"><img src="/img/logo.png" alt="" /></Link></h2>
                <nav className="footerNavigationList">
                    <Link to="/help">Help</Link><span className="delimeter">•</span>
                    <Link to="/press">Press</Link><span className="delimeter">•</span>
                    <Link to="/jobs">Jobs</Link><span className="delimeter">•</span>
                    <Link to="/prohibited-items">Prohibited items</Link><span className="delimeter">•</span>
                    <Link to="/sitemap">Sitemap</Link><span className="delimeter">•</span>
                    <Link to="/contact">Contact</Link>
                </nav>
            </div>
        )
    }
}

export default FooterNavigation
