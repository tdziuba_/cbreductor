/**
 * Created by tdziuba on 05.04.2016.
 */
import React, { Component } from 'react'
import AppOnMailBox from './AppOnMailBox'
import FooterNavigation from './FooterNavigation'
import { getCookie } from '../../helpers/component-helpers'

if (process.browser) {
	require('./footer.scss')
}

class Footer extends Component {

	render() {
		return (
			<div className="cbFooter row">
				{ getCookie('hideBottomApps') ? '' : <AppOnMailBox/> }
				<div className="cbFooterActions row">
					<div className="container row">
						<div className="col col-filled col-7of16 navigation">
							<FooterNavigation />
						</div>
						<div className="col col-filled col-4of16 socialActions">
							<a className="socialIcon fb" href=""><i className="fa fa-facebook" /></a>
							<a className="socialIcon instagram" href=""><i className="fa fa-instagram" /></a>
							<a className="socialIcon yt" href=""><i className="fa fa-youtube" /></a>
						</div>
						<div className="col col-filled col-5of16 appsActions">
							<a href="" className="appButton appStoreBtn">App Store</a>
							<a href="" className="appButton googlePlayBtn">Google Play</a>
						</div>
					</div>
				</div>
				<div className="container copyright">
					<p>Lorem ipsum</p>
					<p>&copy; 2016 Citiboard AB. All rights i tak dalej...</p>
				</div>
			</div>
		)
	}
}

export default Footer
