/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-10
 */

import React, {Component} from 'react'
import axios from 'axios'
import { addEvent, removeEvent, setCookie } from '../../helpers/component-helpers'

class AppOnMailBox extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		let self = this

		addEvent(self.refs.form, 'submit', self.handleSubmit.bind(this))
	}

	componentWillUnmount() {
		let self = this

		removeEvent(self.refs.form, 'submit', self.handleSubmit)
	}

	handleSubmit(ev) {
		ev.preventDefault()

		let data = {
				username: this.refs.username.value,
				email: this.refs.email.value
			}

		if (!data.email || !data.username) {
			alert('Emtpy required field')
		} else {
			//ajax

			axios({
				url: `http://91.240.238.98:9999/offer-details`,
				timeout: 20000,
				method: 'get',
				responseType: 'json',
				withCredentials: false
			}).then(
				response => {
					setCookie('hideBottomApps', 1)
				},
				error => console.warn(error)
			)
		}
	}

	render() {
		return (
			<div className="appOnMailBox row">
				<div className="container">
					<form className="form form-inline row vmid" ref="form">

						<div className="col col-filled col-6of16 col-phablet-16of16">
							<legend>Send me app for iOS or Android:</legend>
						</div>
						<div className="col col-filled col-10of16 col-phablet-16of16">
							<input type="text" className="input input-large" name="username" placeholder="Your Name"
								   ref="username" required/>
							<input type="email" className="input input-large" name="email" placeholder="Your E-mail"
								   ref="email" required/>
							<button className="btn btn-large btn-alternative sendMeAppBtn" ref="submit">Send me the app</button>
						</div>

					</form>
				</div>
			</div>
		)
	}
}

export default AppOnMailBox