/**
 * Created by tdziuba on 11.04.2016.
 */

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { scrollToTop } from '../helpers'

class ScrollToTop extends Component {

	constructor(props) {
		super(props)
	}

	componentDidMount() {

		this.handleButtonVisibility();

		if (window.addEventListener) {

			//scroll event
			window.addEventListener('scroll', this.onScroll.bind(this), false);
		} else {

			//scroll event
			window.attachEvent('on' + 'Scroll', this.onScroll.bind(this));
		}
	}

	componentWillUnmount() {
		if (window.removeEventListener) {
			window.removeEventListener('scroll', this.onScroll);
		} else {
			window.detachEvent('onScroll', this.onScroll);
		}
	}

	handleButtonVisibility() {
		if (!this.hasOwnProperty('btn')) {
			this.btn = ReactDOM.findDOMNode(this)
		}

		if (window.scrollY > (window.outerHeight * 2)) {
			this.btn.className = this.btn.className.replace(' hidden', '')
		} else {
			this.btn.className = this.btn.className.replace(' hidden', '') + ' hidden'
		}

	}

	onScroll() {

		let self = this,
			t

		clearTimeout(t)
		t = setTimeout(function () {
			self.handleButtonVisibility()
		}, 100)
	}

	scrollToPageTop() {
		scrollToTop(0, 250)
	}
	
	render() {
		return (
			<button className="btn btn-small btn-secondary goUpBtn" onClick={ () => { this.scrollToPageTop() } } >
				<i className="fa fa-angle-up"/>
			</button>
		)
	}
}

export default ScrollToTop