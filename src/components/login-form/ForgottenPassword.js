/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-19
 */

import React, {Component} from 'react'
import {addEvent, removeEvent} from '../../helpers/component-helpers'

class ForgottenPassword extends Component {
	constructor(props) {
		super(props)
		this.parent = this.props.parent
		this.error = ''
		this.email = ''
		this.showForm = true
	}

	componentDidMount() {
		addEvent( this.refs.email, 'keyup', this.emailOnKeyup.bind(this) )
		addEvent( this.refs.sendLinkBtn, 'click', this.onSubmit.bind(this) )
		addEvent( this.refs.form, 'submit', this.onSubmit.bind(this) )
		addEvent( this.refs.clearEmail, 'click', this.clearEmail.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.email, 'keyup', this.emailOnKeyup )
		removeEvent( this.refs.sendLinkBtn, 'click', this.onSubmit )
		removeEvent( this.refs.form, 'click', this.onSubmit )
		removeEvent( this.refs.clearEmail, 'click', this.clearEmail )
	}
	
	showSuccess(bool) {
		if (bool) {
			this.showForm = false
		}
		
		return this
	}

	emailOnKeyup(ev) {
		this.email = ev.target.value
	}

	clearEmail(ev) {
		this.refs.email.value = ''
	}

	onSubmit(ev) {
		ev.preventDefault()
		let isValid = this.validateFrom()

		if (isValid) {
			this.parent.submitForgottenForm(this.email)
		} else {
			this.forceUpdate()
		}
	}

	validateFrom() {

		if (!this.refs.email.value) {
			this.error = 'Please fill the email field'
		} else {
			this.error = ''
		}

		return (this.error === '')
	}

	renderForm() {

		return (
			<section className="forgottenPasswordSection formSection">
				<h3>Forgotten password</h3>
				<p>Enter your e-mail adress and we will send you a new password</p>
				<form className="forgottenForm form" ref="form">

						<div className={ `formRow ${ this.error ? 'error' : '' }` }>
							<strong>{ this.error }</strong>
							<label>
								<i className="icon icon-envelope" aria-hidden="true"/>
								<input type="email" id="email" placeholder="Email"
									   className="input input-large emailInput" ref="email"/>
								<i className="icon icon-circle-x" aria-hidden="true" ref="clearEmail"/>
							</label>
						</div>

						<button className="btn btn-large btn-primary sendLinkBtn" ref="sendLinkBtn">
							<span>Send reset link</span>
						</button>

				</form>
			</section>
		)

	}

	renderConfirmation() {
		return (
			<section className="linkSentSection forgottenPasswordSection formSection">
				<i className="icon icon-face-happy" aria-hidden="true" />
				<h3>Sent!</h3>
				<p>Check your e-mail for password.</p>
			</section>
		)
	}

	render() {
		let content = (this.showForm) ? this.renderForm() : this.renderConfirmation()

		return content

	}
}

export default ForgottenPassword
