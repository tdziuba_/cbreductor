/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-19
 */

import React, { Component } from 'react'
import axios from 'axios'

class RegisterForm extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		let loc = {
			lat: 0,
			lng: 0
		}


		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				loc.lat = position.coords.latitude
				loc.lng = position.coords.longitude
			})
		} else {

			axios.get('https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAzcgAJPJsQqObMBXgbDhDSQDyCH8HHJy8', {
				withCredentials: true
			}).then((response) => {

				let geo = new google.maps.Geocoder()
			})
		}
	}

	render() {
		const {children} = this.props

		return (
			<section className="registerFormSection">
				<h3>Register using:</h3>
				<a className="btn btn-large fbLoginBtn" ref={ (ref) => this.fbLoginBtn = ref }><i className="fa fa-facebook" aria-hidden="true"/> Facebook</a>
				<form>
					<fieldset>
						<legend>or using your e-mail address:</legend>
						<div className="formRow">
							<input type="email" placeholder="Email" className="input input-medium emailInput" />
						</div>
						<div className="formRow">
							<input type="password" placeholder="Password" className="input input-medium passwordInput" />
						</div>
						<input type="hidden" lat="" ref="latInput" />
						<input type="hidden" lng="" ref="lngInput" />

						<button className="btn btn-large btn-primary rgisterBtn">Registera</button>
					</fieldset>
				</form>
			</section>
		)
	}
}

export default RegisterForm