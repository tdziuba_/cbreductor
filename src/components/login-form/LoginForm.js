/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-19
 */

import React, { Component } from 'react'
import { Link } from 'react-router'
import { addEvent, removeEvent } from '../../helpers/component-helpers'

class LoginForm extends Component {
	constructor(props) {
		super(props)
		this.parent = null
		this.isPasswordVisible = false
		this.oldPass = ''
		this.oldEmail = ''
		this.oldPlaceholder = ''
		this.errors = {
			pass: '',
			email: ''
		}
	}

	componentDidMount() {
		this.oldPlaceholder = 'Password'

		//eventy
		addEvent( this.refs.fbLoginBtn, 'click', this.loginByFb.bind(this) )
		addEvent( this.refs.loginBtn, 'click', this.loginByEmail.bind(this) )
		addEvent( this.refs.form, 'submit', this.loginByEmail.bind(this) )
		addEvent( this.refs.password, 'keyup', this.onPasswordKeyup.bind(this) )
		addEvent( this.refs.email, 'keyup', this.onEmailKeyup.bind(this) )
		addEvent( this.refs.email, 'focus', this.onEmailFocus.bind(this) )
		addEvent( this.refs.clearEmail, 'click', this.clearEmail.bind(this) )
		addEvent( this.refs.showPassword, 'click', this.showPassword.bind(this) )
		addEvent( this.refs.clearPassword, 'click', this.clearPassword.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.fbLoginBtn, 'click', this.loginByFb )
		removeEvent( this.refs.loginBtn, 'click', this.loginByEmail )
		removeEvent( this.refs.form, 'submit', this.loginByEmail )
		removeEvent( this.refs.password, 'keyup', this.onPasswordKeyup )
		removeEvent( this.refs.email, 'keyup', this.onEmailKeyup )
		removeEvent( this.refs.email, 'focus', this.onEmailFocus )
		removeEvent( this.refs.clearEmail, 'click', this.clearEmail )
		removeEvent( this.refs.showPassword, 'click', this.showPassword )
		removeEvent( this.refs.clearPassword, 'click', this.clearPassword )
	}

	componentWillReceiveProps() {
		this.parent = this.props.parent
	}

	loginByFb(ev) {
		this.props.parent.loginByFb()
	}

	loginByEmail(ev) {
		ev.preventDefault();

		let isValid = this.validateFrom()

		if (isValid) {
			this.parent.submitLoginForm(this.oldEmail, this.oldPass)
		} else {
			this.forceUpdate()
		}
	}

	validateFrom() {

		if (!this.refs.password.value) {
			this.errors.pass = 'Please fill the password field'
		} else {
			this.errors.pass = ''
		}

		if (!this.refs.email.value) {
			this.errors.email = 'Please fill the email field'
		} else {
			this.errors.email = ''
		}

		return (this.errors.email === '' && this.errors.pass === '')
	}

	onPasswordKeyup(ev) {
		this.oldPass = ev.target.value

		let row = this.refs.password.parentNode.parentNode

		row.className = row.className.replace('error', '')
		this.refs.clearPassword.className = this.refs.clearPassword.className.replace('hidden', '')
	}

	onEmailKeyup(ev) {
		this.oldEmail = ev.target.value
		console.log(ev.target.value);
		let row = this.refs.email.parentNode.parentNode
		debugger;

		row.className = row.className.replace('error', '')
	}

	onEmailFocus() {
		this.refs.form.className = this.refs.form.className.replace('short', 'full')
	}

	clearEmail(ev) {
		this.refs.email.value = ''
	}

	showPassword() {

		if (!this.isPasswordVisible) {

			// this.refs.password.placeholder = this.oldPass
			this.refs.password.type = 'text'
			this.refs.password.focus()
			this.isPasswordVisible = true

		} else {

			this.refs.password.type = 'password'
			this.refs.password.focus()
			this.isPasswordVisible = false

		}
	}

	clearPassword(ev) {
		ev.target.className = ev.target.className + ' hidden'
		this.refs.password.value = ''
		this.refs.password.focus()
	}

	showForgotten() {
		this.props.parent.displayForgottenForm()
	}

	render() {

		return (
			<section className="loginFormSection formSection">
				<h3>Log in using:</h3>
				<a className="btn btn-large fbLoginBtn" ref="fbLoginBtn"><i className="icon icon-fb" aria-hidden="true"/>Facebook</a>
				<form className="loginForm form short" ref="form">
					<fieldset className="fieldset" ref="fieldset">
						<legend>or using your e-mail address:</legend>
						<div className={ `formRow ${ this.errors.email ? 'error' : '' }` }>
							<strong>{ this.errors.email }</strong>
							<label>
								<i className="icon icon-envelope" aria-hidden="true" />
								<input type="email" id="email" placeholder="Email" className="input input-large emailInput" ref="email" />
								<i className="icon icon-circle-x" aria-hidden="true" ref="clearEmail" />
							</label>
						</div>
						<div className={ `formRow ${ this.errors.pass ? 'error' : '' }` }>
							<strong>{ this.errors.pass }</strong>
							<label>
								<i className="icon icon-lock" aria-hidden="true" />
								<input type="password" id="password" placeholder={ this.oldPlaceholder } className="input input-large passwordInput" ref="password" />
								<i className="icon icon-eye" aria-hidden="true" ref="showPassword" />
								<i className="icon icon-circle-x hidden" aria-hidden="true" ref="clearPassword" />
							</label>
						</div>

						<button className="btn btn-large btn-primary loginBtn" ref="loginBtn"><span>Log in</span></button>
					</fieldset>
				</form>
				<Link className="forgotPassword" ref="forgotten" to="/access/forgotten-password">Forgot your password?</Link>
			</section>
		)
	}
}

export default LoginForm