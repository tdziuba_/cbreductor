/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-09
 */

import React, { Component } from 'react'

class OfferFormPictureSlot extends Component {
	constructor(props) {
		super(props)
		this.parent = null
	}

	componentDidMount() {
		this.parent = this.props.parent;
	}

	handleUpload() {
		let { parent, num } = this.props

		parent.onOpenClick(num)
	}
	
	generateCanvas(data) {
		let reader = new FileReader(),
			self = this

		this.refs.slot.className = this.refs.slot.className + ' loading'

		reader.onload = function (e) {
			self.picture = e.target.result
			self.refs.slot.className = self.refs.slot.className.replace(' loading', '')
			self.refs.slotImg.className = self.refs.slotImg.className.replace(' hidden', '')

			self.parent.slotShouldBeUpdated(self)
			self.forceUpdate()
		}

		reader.readAsDataURL(data)
	}

	render() {
		const { isActive, picture, num } = this.props
		let self = this

		return (
			<div id={`picSlot-${num}`} className={`pictureSlot ${ isActive ? 'current' : '' }`} onClick={this.handleUpload.bind(this)} ref="slot">
				{ isActive ? <div className="addPhotoLabel"><i className="fa fa-camera" aria-hidden="true"/><span>Add Photo</span> </div> : '' }
				<img className="slotImage hidden" src={self.picture ? self.picture : ''} alt=""ref="slotImg" />
			</div>
		)
	}
}

export default OfferFormPictureSlot