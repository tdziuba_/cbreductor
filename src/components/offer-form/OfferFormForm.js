/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-09
 */

import React, { Component } from 'react'
import FakeRadio from '../../components/common/forms/FakeRadio'
import FakeSelect from '../../components/common/forms/FakeSelect'
import InputCounter from '../../components/common/forms/InputCounter'

class OfferFormForm extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		
		let json = [
			{ cat: 'Electronics', catId: 1 },
			{ cat: 'Mode', catId: 2 },
			{ cat: 'Hem & Inredning', catId: 3 }
		]
		
		let list = json.map(function (item) {
			return (
				<span key={ item.catId } data-value={ item.catId } data-text={ item.cat }>{ item.cat }</span>
			)
		})

		return (
			<form className="form offerFormForm">
				<div className="formRow">
					<FakeRadio value="used" isChecked={true} label="Used" />
					<span className="label label-default">or</span>
					<FakeRadio value="new" isChecked={false} label="New" />
				</div>
				<div className="formRow">
					<FakeSelect firstLabel="Electronic" inputName="category">{ list }</FakeSelect>
				</div>
				<div className="formRow">
					<InputCounter max="128">
						<input className="input input-large" type="text" maxLength="128" placeholder="Title" name="title" />
					</InputCounter>
				</div>
				<div className="formRow">
					<InputCounter max="256">
						<textarea className="input input-large textarea" type="text" maxLength="256" placeholder="Description" name="description" />
					</InputCounter>
				</div>
				<div className="formRow">
					<label><input type="text" className="input input-large priceInput" name="price" placeholder="Price"/><strong>kr</strong></label>
				</div>
				<div className="formRow">
					<label><input type="checkbox" className="checkbox" defaultChecked={true}/><span>Share on Facebook</span></label>
				</div>
			</form>
		)
	}
}

export default OfferFormForm