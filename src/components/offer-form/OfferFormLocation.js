/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-09
 */

import React, {Component} from 'react'

class OfferFormLocation extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const {children} = this.props

		return (
			<div>
				{ children }
			</div>
		)
	}
}

export default OfferFormLocation