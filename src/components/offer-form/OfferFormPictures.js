/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-09
 */

import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import OfferFormPictureSlot from './OfferFormPictureSlot'
import { addEvent, removeEvent } from '../../helpers/component-helpers'
import axios from 'axios'

class OfferFormPictures extends Component {
	constructor(props) {
		super(props)
		this.activeSlot = 0
		this.files = []
		this.slots = []
	}

	componentDidMount() {
		addEvent(ReactDOM.findDOMNode(this.refs.fileUploader), 'change', this.handleFileUploader.bind(this))
	}

	componentWillUnmount() {
		removeEvent(ReactDOM.findDOMNode(this.refs.fileUploader), 'change', this.handleFileUploader.bind(this))
	}

	handleFileUploader(ev) {
		let data = new FormData(),
			self = this, i

		this.files = ev.target.files

		for (i in this.files) {
			data.append('file', this.files[i])
		}

		axios({
			url: `http://91.240.238.98:9999/upload`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			withCredentials: false,
			data: data
		}).then(
			response => {
				self.generateImages()
			},
			error => console.log(error)
		)

	}

	onOpenClick() {
		this.refs.fileUploader.click();
	}

	getSlots() {
		const { pictures } = this.props
		let self = this,
			slots = [],
			i

		for (i = 0; i < 5; i++) {
			slots.push(<OfferFormPictureSlot key={i} parent={self} picture={ null } ref={`slot-${i}`} isActive={ (self.activeSlot === i) } num={i} />)
		}

		this.slots = slots

		return slots
	}

	generateImages() {
		let self = this,
			i, slot

		for (i =0; i < this.files.length; i++) {
			slot = this.refs['slot-'+this.activeSlot]

			slot.generateCanvas(this.files[i])

			this.activeSlot++
		}
	}

	slotShouldBeUpdated(slot) {
		let self = this,
			props = slot.props
		this.forceUpdate()
	}

	render() {

		return (
			<div className="offerFormPictures">
				{ this.getSlots() }
				<form encType="multipart/form-data" className="hidden">
					<input type="file" ref="fileUploader" name="fileUploader" multiple />
				</form>

			</div>
		)
	}
}

export default OfferFormPictures