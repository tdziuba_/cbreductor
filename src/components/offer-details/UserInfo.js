/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-12
 */

import React, { Component } from 'react'
import { Link } from 'react-router'

class UserInfo extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { data } = this.props

		return (
			<div className="userInfoBox">
				<h3>About the seller:</h3>
				<Link className="avatar" to={`/personen-${ data.owner_id }`}><img className="avatarImg" src={ data.owner_avatar } alt={ data.owner_name }/></Link>
				<Link className="owner" to={`/personen-${ data.owner_id }`}>{ data.owner_name }</Link>
				<Link className="location" to={`/jakas-lokacja`}>{ data.location_info }</Link>
			</div>
		)
	}
}

export default UserInfo
