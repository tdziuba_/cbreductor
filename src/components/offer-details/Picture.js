/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-19
 */

import React, {Component} from 'react'
import OfferGallery from './OfferGallery'

class Picture extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { data } = this.props

		return (
			<div className="picture">
				<figure>
					<OfferGallery data={ data }/>

					<figcaption>
						<h3>Description:</h3>
						<p>Size Large (10/12) Machine washable Worn only twice</p>
					</figcaption>
				</figure>
			</div>
		)
	}
}

export default Picture