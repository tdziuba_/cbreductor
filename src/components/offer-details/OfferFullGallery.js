/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-12
 */

import React, { Component } from 'react'
import { addEvent, removeEvent, swipeLeft, swipeRight } from '../../helpers'
if (process.browser) {
	require('tocca')
}

class OfferFullGallery extends Component {
	constructor(props) {
		super(props)

		this.isHidden = false
	}

	componentDidMount() {
		addEvent(this.refs.mainPhoto, 'swipeleft', this.handlePrevImageChange.bind(this))
		addEvent(this.refs.mainPhoto, 'swiperight', this.handleNextImageChange.bind(this))
		addEvent(this.refs.layer, 'click', this.hide.bind(this))
	}

	componentWillUnmount() {
		removeEvent(this.refs.layer, 'click', this.hide)
	}

	hide() {
		this.isHidden = true
		this.forceUpdate();
	}

	show() {
		this.isHidden = false
		this.forceUpdate();
	}

	handleNextImageChange() {
		alert('NEXT image, please!');
	}

	handlePrevImageChange() {
		alert('PREV image, please!');
	}

	render() {
		const { data } = this.props
		let isHidden = this.isHidden

		return (
			<div className={`offerFullGallery${ isHidden? ' hidden' : ' fullscreen'}`} ref="offerFullGallery">
				<div className="layer" ref="layer" />
				<div className="picWrp" ref="picWrp">
					<div className="thumbnailList"></div>
					<div className="mainPhotoBox" ref="mainPhotoBox">
						<img src={ data.picture.big } className="mainPhoto" ref="mainPhoto" alt="" />
					</div>

				</div>
			</div>
		)
	}
}

export default OfferFullGallery
