/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-13
 */

import React, { Component } from 'react'
import { Link } from 'react-router'
import OfferGridItem from '../offers/OfferGridItem'

class UserOffers extends Component {
    constructor(props) {
        super(props)
    }
    
    render() {
        const { data } = this.props
        let fakeData1 = {
                id: '12345',
                'location_info': 'Västra Götaland, Svenljunga',
                title: 'Barnstol \'svan of sweden\' tripptrapp',
                price: 500,
                'pictures_count': 1,
                picture: {
                    full: 'http://cbsethumb.blob.core.windows.net/534x389/a9/e9/af/barnstol-svan-of-sweden-tripptrapp-5756275.jpg'
                }
            },
            fakeData2 = {
                id: '43256',
                'location_info': 'Västra Götaland, Svenljunga',
                title: 'Volvo 855 2,0 -96',
                price: 4500,
                'pictures_count': 1,
                picture: {
                    full: 'http://cbsethumb.blob.core.windows.net/534x389/42/b2/6f/volvo-855-2-0-96-arsmodell-1996-matarstallning-28500-bensin-manuell-rod-gavle-soderhamn-hudiksvall-sandviken-6241393.jpg'
                }
            }
        
        return (
            <div className="usersOffers">
                <h3>{ data.owner_name }'s offers:</h3>
                
                <section className="ownerOffers">
                    <OfferGridItem data={fakeData1} />
                    <OfferGridItem data={fakeData2} />
                </section>

                <Link to={`/personen-${ data.owner_id }`} className="profileLink"><strong>See all { data.owner_name }'s offers</strong><i className="fa fa-angle-right" aria-hidden="true" /></Link>
            </div>
        )
    }
}

export default UserOffers
