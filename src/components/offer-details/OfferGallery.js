/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-12
 */

import React, {Component} from 'react'
import { addEvent, removeEvent } from '../../helpers/component-helpers'
import OfferFullGallery from './OfferFullGallery'

class OfferGallery extends Component {
	constructor(props) {
		super(props)
		this.showLayer = false
	}

	componentDidMount() {
		addEvent(this.refs.btnPrev, 'click', this.changeImage.bind(this))
		addEvent(this.refs.offerPicture, 'click', this.showFullGallery.bind(this))
	}

	componentWillUnmount() {
		removeEvent(this.refs.btnPrev, 'click', this.changeImage)
	}

	renderThumbnails() {
		return (
			<div className="thumbnails">
				<img className="thumbnail" src="" />
			</div>
		)
	}

	showFullGallery(ev) {

		this.showLayer = true
		this.forceUpdate()

	}

	changeImage() {
		alert('change image now!');
	}

	render() {
		const { data } = this.props
		let isMore = data.pictures_count > 1 && data.picture.hasOwnProperty('big')

		return (
			<div className="pictWrapper">
				<img itemProp="image" className="offerPicture" ref="offerPicture"
					 src={ data.picture.hasOwnProperty('big') ? data.picture.big : '/img/responsive/no-picture.png' }
					 alt={ data.title } />
				{ isMore ? <a className="btnPrev" ref="btnPrev"><span className="btn"><i className="fa fa-angle-left" aria-hidden="true" /></span></a> : '' }
				{ isMore ? <a className="btnNext" ref="btnNext"><span className="btn"><i className="fa fa-angle-right" aria-hidden="true" /></span></a> : '' }
				{ isMore ? this.renderThumbnails() : '' }
				{ this.showLayer ? <OfferFullGallery ref="offerFullGallery" data={data} /> : '' }
			</div>
		)
	}
}

export default OfferGallery
