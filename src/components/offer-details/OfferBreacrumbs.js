/**
 * Created by tdziuba on 14.04.2016.
 */
import React, {Component} from 'react';
import { urlFriendlyString, removeDiacritics } from '../../helpers'
import { Link } from 'react-router'

class OfferBreacrumbs extends Component {
	constructor(props) {
		super(props)
		this.data = {}
	}

	componentDidMount() {
		this.data = this.props.data
	}

	renderLocationLink(location, fullUrl) {
		let url = (!fullUrl) ? '/' + removeDiacritics(location) : '/' + fullUrl

		return (
			<Link to={url} title={location}>{location}</Link>
		)

	}

	render() {

		var data = this.props.data,
			location, locationUrl, date;

		if (data) {
			date = (data.hasOwnProperty('creation_date')) ? data.creation_date.split(' ') : null
			location = (data.hasOwnProperty('location_info')) ? data.location_info.split(', ') : ''
			locationUrl = (location) ? (location.length > 1) ? removeDiacritics(location[0].trim()) + '/' + removeDiacritics(location[1].trim()) : removeDiacritics(data.location_info) : ''

		}

		return (
			<div className="breadcrumbs col col-16of16">
				<Link to="/">Home</Link><span className="delimeter"><i className="fa fa-angle-right" aria-hidden="true" /></span>
				<Link to="/">Fashion</Link><span className="delimeter"><i className="fa fa-angle-right" aria-hidden="true" /></span>
				<Link to="/">Dress</Link><span className="delimeter"><i className="fa fa-angle-right" aria-hidden="true" /></span>
				<Link to="/">Halland</Link><span className="delimeter"><i className="fa fa-angle-right" aria-hidden="true" /></span>
				<Link to="/">Halmstad</Link>
			</div>
		)
	}
}

export default OfferBreacrumbs