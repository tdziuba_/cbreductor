/**
 * Created by tdziuba on 14.04.2016.
 */
import React, {Component} from 'react';

import OfferBreadcrumbs from './OfferBreacrumbs'
import OfferInfo from './OfferInfo'
import UserInfo from './UserInfo'
import UserOffers from './UserOffers'
import Picture from './Picture'

if (process.browser) {
	require('./offer-details.scss')
}

class OfferDetailsComponent extends Component {
	constructor(props) {
		super(props)
	}

	render() {

		const { data } = this.props

		return (
			<div className="offerDetailsContainer">
				<div className="row vtop">
					<OfferBreadcrumbs data={data} />
					<div className="col col-9of16">
						<div className="offerDetailsPrimaryContent">
							<Picture data={data} />
						</div>
					</div>
					<div className="col col-5of16">
						<div className="offerDetailsSecondaryContent">
							<OfferInfo data={data} />
							<UserInfo data={data} />
							<UserOffers data={data} />
						</div>
					</div>
				</div>
			</div>
		)
	}


// 	render() {
//
// 		const { data } = this.props
// console.log(data);
// 		return (
// 			<article itemScope="" itemType="http://schema.org/Product" className="offerDetailsBox gridItem">
// 				<meta itemProp="name" content={data.title}/>
// 				<div itemProp="offers" itemScope="" itemType="http://schema.org/Offer" className="main clearfix">
//
// 					{data ? <OfferHeader data={data} /> : ''}
//
// 					<Picture data={data} />
//
// 					<div className="offerDetailsInfo">
// 						<div className="offerAdditionalInfoBox">
//
// 							<div className="contactWrapper">
//
// 								<section className="contact row">
// 									<div className="ownerInfoWrp hasPhone">
//
// 										<div itemProp="seller" itemScope="" itemType="http://schema.org/Person"
// 											 className="ownerInfo col col-1of2">
// 											<p>
// 												<a href={`/personen-${data.owner_id}`}>
// 													<span className="avatar fl">
// 														<img className=""
// 															 src={data.owner_avatar}
// 															 alt={data.owner_name} />
// 													</span>
// 													<strong itemProp="name" className="name truncated"><span>{data.owner_name}</span></strong>
// 												</a>
// 											</p>
// 											<p className="seeOtherPersonBoardItems col col-1of2">
// 												<a itemProp="url" href={`/personen-${data.owner_id}`}>Alla annonser</a>
// 											</p>
//
// 										</div>
//
// 										<a itemProp="telephone" href="tel:0704423208" className="phone"><i className="fa fa-mobile" aria-hidden="true"/> 0704423208</a>
// 									</div>
// 									<div className="askSeller">
//
// 										<a className="btn btnS01 btnC01 sendPrivateMessageBtn showDoorLayer"
// 										   href="javascript:;"
// 										   data-loginurl="/nykoping-oxelosund/vogel-tv-vaggfaste,2941119?fb_connect=1&amp;act=offer&amp;obj=2941119"
// 										   data-simpleloginurl="/nykoping-oxelosund/vogel-tv-vaggfaste,2941119"
// 										   data-afterlogindestclicksel="#boardItem-2941119 .sendPrivateMessageBtn"
// 										   data-afterloginactionlabel="item-message">
// 											<i className="icon icon_envelope"></i><span className="text">Sänd privat meddelande</span>
// 										</a>
//
// 									</div>
// 								</section>
//
// 							</div>
//
// 							<div className="cntDescriptionWrp">
// 								<div className="description readable">
// 									<p itemProp="description">
// 										Vogel tv-väggfäste för mindre tv-apparater.<br />
// 										15-32 tum</p>
// 								</div>
//
// 							</div>
//
//
// 							<div className="actions">
// 								<ul>
// 									<li className="favoriteOffer">
//
// 										<a className="favoriteOfferLink favoriteOfferAddLink showDoorLayer"
// 										   title="Spara annons" data-bid="2941119" data-type="add"
// 										   data-loginurl="/nykoping-oxelosund/vogel-tv-vaggfaste,2941119?fb_connect=1&amp;act=offer&amp;obj=2941119"
// 										   data-simpleloginurl="/nykoping-oxelosund/vogel-tv-vaggfaste,2941119"
// 										   data-afterlogindestclicksel="#boardItem-2941119 .favoriteOfferLink"
// 										   data-afterloginactionlabel="add-to-favorites">
// 											<i className="icon icon_favorite_add"></i>Spara annons </a>
// 									</li>
// 									<li>
// 										<div className="fb-share-button shareItemOnFb fb_iframe_widget"
// 											 data-item_id="2941119"
// 											 data-href="/l/i,2941119"
// 											 data-layout="button" />
// 									</li>
// 									<li>
// 										<a class="twitter-share-button"
// 										   href="https://twitter.com/intent/tweet?text=Hello%20world"
// 										   data-size="large">
// 											Tweet</a>
// 									</li>
// 								</ul>
//
// 							</div>
//
//
// 							<div className="applicationButtons">
// 								<h3>Ladda ner våra appar gratis!</h3>
// 								<a href="https://itunes.apple.com/se/app/citiboard-ab/id628478527?mt=8"
// 								   className="appBtn appStoreBtn" target="_blank"></a>
// 								<a href="https://play.google.com/store/apps/details?id=se.citiboard"
// 								   className="appBtn googlePlayBtn" target="_blank"></a>
// 							</div>
//
// 						</div>
//
// 						<div className="commentsBoxWrp">
//
// 							<div id="boardItemComments-2941119" className="commentsBox commentsTo_2941119">
//
// 								<div className="comments commentsSection_2941119">
// 								</div>
// 								<div className="commentFormWrp">
// 									<div className="inpSet01">
// 										<a className="placeHolder inpL01 showDoorLayer" href="javascript:;"
// 										   data-loginurl="/i,2941119?fb_connect=1&amp;act=offer&amp;obj=2941119"
// 										   data-simpleloginurl="/i,2941119"
// 										   data-afterlogindestclicksel=" #boardItemComments-2941119 .commentInputArea"
// 										   data-afterloginactionlabel="item-show-comment">
// 											Kommentera, ställ en fråga eller lägg ett bud... </a>
// 									</div>
// 								</div>
// 							</div>
// 						</div>
// 					</div>
//
//
// 				</div>
//
// 			</article>
// 		)
// 	}

}

export default OfferDetailsComponent