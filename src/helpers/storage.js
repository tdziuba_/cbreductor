/**
 * Created by tdziuba on 2016-05-20.
 */
const Storage = {
	set(key, value) {
		try {
			if (typeof sessionStorage !== 'undefined') {
				sessionStorage.setItem(key, value)
			}
		} catch (er) {/* do nothing - private mode */}
	},

	get(key) {
		try {
			if (typeof sessionStorage !== 'undefined') {
				sessionStorage.getItem(key)
			}
		} catch (er) {/* do nothing - private mode */}
	},

	del(key) {
		try {
			if (typeof sessionStorage !== 'undefined') {
				sessionStorage.removeItem(key)
			}
		} catch (er) {/* do nothing - private mode */}
	}
}

export default Storage
