/**
 * Created by tdziuba on 2016-04-25.
 */

export function addEvent(element, eventType, callback) {
	try {
		if (element) {
			if (element.addEventListener) {
				element.addEventListener(eventType, callback, false)
			} else {
				element.attachEvent('on' + eventType, callback)
			}
		}
	} catch (err) {
		console.warn(err, element)
	}
}

export function removeEvent(element, eventType, callback) {
	if (element) {
		if (element.removeEventListener) {
			element.removeEventListener(eventType, callback)
		} else {
			element.detachEvent('on' + eventType, callback)
		}
	}
}

export function formatPrice(price) {

	return parseFloat(price).toLocaleString('se-SE') + ' kr'
}

export function getCookie(name) {
	if ( typeof document !== 'undefined' && document.hasOwnProperty('cookie') ) {
		let text = RegExp('' + name + '[^;]+').exec(document.cookie)

		return decodeURIComponent(!!text ? text.toString().replace(/^[^=]+./, '') : '')
	} else {
		return
	}
}

export function setCookie(name, value) {
	if ( typeof document !== 'undefined' && document.hasOwnProperty('cookie') ) {
		document.cookie = name + '=' + value + ''
	}
}

export function deleteCookie(name, value) {
	if ( typeof document !== 'undefined' && document.hasOwnProperty('cookie') ) {
		document.cookie = name + '=' + value + ''
	}
}

export function removeCookie(name, value) {
	deleteCookie(name, value)
}

