/**
 * Created by tdziuba on 11.04.2016.
 */

function scrollToEl(el, to, speed) {
	if (speed <= 0) {
		let msg = 'function scrollTo needs 3rd argument speed to be highter then 0'

		if (typeof console && typeof console.warn === 'function') {
			console.warn(msg);
		} else {
			throw Error(msg);
		}

		return;
	}

	let top = el.getBoundingClientRect().top,
		diff = to - el.getBoundingClientRect().top,
		distance = diff / speed * 10,
		t;

	t = setTimeout(function() {
		top += distance

		if (top <= to) {
			clearTimeout(t)
		}

		scrollToEl(el, to, speed - 10)

	}, 10);
}

export default function scrollToTop (top, speed) {

	let interval = (speed / 25),
		distance = window.scrollY / interval,
		t

	t = setInterval(function () {
		distance = (window.scrollY - (distance / interval))

		if (distance <= top) {
			clearInterval(t)
			distance = 0
		}

		window.scrollTo(0, distance)
	}, interval)
}