/**
 * Created by tdziuba on 06.04.2016.
 */

import React from 'react'

import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import createLogger from 'redux-logger'
import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'

import { routerReducer, routerMiddleware } from 'react-router-redux'

import * as reducers from '../reducers'

export const DevTools = createDevTools(
	<DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
		<LogMonitor theme="tomorrow" preserveScrollTop={false} />
	</DockMonitor>
)

export function configureStore(history, initialState) {
	const reducer = combineReducers({
		...reducers,
		routing: routerReducer
	})

	let devTools = []
	if (typeof document !== 'undefined') {
		devTools = [ DevTools.instrument() ]
	}

	const store = createStore(
		reducer,
		initialState,
		compose(
			applyMiddleware(
				routerMiddleware(history),
				createLogger()
			),
			typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
		)
	)

	return store
}
