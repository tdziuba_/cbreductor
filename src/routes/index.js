/**
 * Created by tdziuba on 01.04.2016.
 */

import React from 'react'
import { Route, IndexRoute } from 'react-router'
import * as c from '../containers'
import { NotFound, Error } from '../components'
import store from '../store'


const routes = (
	<Route path="/" component={c.App} store={store} >
		<IndexRoute component={c.Feed} store={store} />
		<Route path="/:location/:title,:id" component={c.Details} store={store} />
		<Route path="profile" component={c.Profile} store={store} />
		<Route path="favourites" component={c.Favourites} store={store} />
		<Route path="access" component={c.AccessForm} store={store}>
			<Route path=":action" component={c.AccessForm} store={store} />
		</Route>
		<Route path="add-offer" component={c.OfferForm} store={store}>
			<Route path=":action" component={c.OfferForm} store={store} />
		</Route>
		<Route path="edit-offer" component={c.OfferForm} store={store}>
			<Route path=":action" component={c.OfferForm} store={store} />
		</Route>
		<Route path="error" component={Error} />
		<Route path="*" component={NotFound} />
	</Route>
)

export default routes






















// import { React, Component } from 'react';
// import {IndexRoute, Route} from 'react-router';
// import { Provider } from 'react-redux'
// import AppContainer from '../containers/AppContainer'
// import Feed from '../containers/FeedContainer'
// import Profile from '../containers/ProfileContainer'
// import NotFound from '../components/NotFound'
// import store from '../store'
//
// const AppRoutes = {
// 	component: 'div',
// 	childRoutes: [ {
// 		path: '/',
// 		component: AppContainer,
// 		indexRoute: require('./Feed'),
// 		childRoutes: [
// 			//require('./Feed'),
// 			//require('./Dashboard'),
// 			require('./Profile'),
// 			require('./Error')
// 		]
// 	} ]
// };
//
// class RootRoute extends Component {
// 	constructor(props) {
// 		super(props);
// 	}
//
// 	render() {
// 		const { history, store } = this.props
//
// 		return (
// 			<Provider store={store} key="provider" >
// 				<Router history={history} routes={AppRoutes} store={store} />
// 			</Provider>
// 		)
// 	}
// };
//
// //export default RootRoute;
// export { RootRoute, AppRoutes };


//console.log(rootRoute);
// export default function createRootRoute (store, history) {
// 	render (
// 		<Provider store={store} key="provider" >
// 			<Router history={history} routes={rootRoute} store={store} />
// 		</Provider>
// 	)
// }

// export default function rootRoute (store) {
//
// 	render() {
// 		return (
// 			<Route path="/" component={AppContainer}>
// 				{ /* Home (main) route */ }
// 				<IndexRoute component={Feed}/>
//
// 				{ /* Routes requiring login */ }
// 				{ /*
// 				 <Route onEnter={requireLogin}>
// 				 <Route path="chat" component={Chat}/>
// 				 <Route path="loginSuccess" component={LoginSuccess}/>
// 				 </Route>
// 				 */ }
//
// 				{ /* Routes */ }
// 				<Route path="profile" component={Profile}/>
//
// 				{ /* Catch all route */ }
// 				<Route path="*" component={NotFound} status={404}/>
// 			</Route>
// 		)
// 	}
// }