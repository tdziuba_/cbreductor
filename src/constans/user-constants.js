/**
 * Created by tdziuba on 2016-05-05.
 */

export const REQ_LOGOUT_USER = 'REQ_LOGOUT_USER';
export const RECV_LOGOUT_USER = 'RECV_LOGOUT_USER';
export const REQ_LOGIN_USER = 'REQ_LOGIN_USER';
export const RECV_LOGIN_USER = 'RECV_LOGIN_USER';