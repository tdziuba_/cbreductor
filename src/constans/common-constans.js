/**
 * Created by tdziuba on 31.03.2016.
 */

export const RECV_ERROR = 'RECV_ERROR';
export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE'
export const REQ_DATA = 'REQ_DATA';
export const RECV_DATA = 'RECV_DATA';

export const SET_LAYOUT_MODE = 'SET_LAYOUT_MODE'
export const GET_LAYOUT_MODE = 'GET_LAYOUT_MODE'

export const SET_OVERLAY = 'SET_OVERLAY'
export const GET_OVERLAY = 'SET_OVERLAY'
export const SET_DIALOG = 'SET_DIALOG'
export const GET_DIALOG = 'GET_DIALOG'
export const SET_TOOLTIP = 'SET_TOOLTIP'
export const GET_TOOLTIP = 'GET_TOOLTIP'