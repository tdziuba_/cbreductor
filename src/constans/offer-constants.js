/**
 * Created by tdziuba on 31.03.2016.
 */

export const REQ_ALL_OFFER = 'REQ_ALL_OFFER';
export const RECV_ALL_OFFER = 'RECV_ALL_OFFER';
export const RECV_ALL_OFFER_ERROR = 'RECV_ALL_OFFER_ERROR';
export const REQ_MORE_OFFERS = 'REQ_MORE_OFFERS';
export const RECV_MORE_OFFERS = 'RECV_MORE_OFFERS';

//single offer
export const REQ_OFFER = 'REQ_OFFER';
export const RECV_OFFER = 'RECV_OFFER';
export const ADD_OFFER = 'ADD_OFFER';
export const DELETE_OFFER = 'DELETE_OFFER';
export const EDIT_OFFER = 'EDIT_OFFER';

export const ADD_TO_FAVOURITES = 'ADD_TO_FAVOURITES';
export const REMOVE_FROM_FAVOURITES = 'REMOVE_FROM_FAVOURITES';