import 'babel-polyfill'

import React from 'react'
import ReactDOM, { render } from 'react-dom'

import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import { configureStore } from './store'
import routes from './routes'

if (process.browser) {
	require('./scss/common.scss')
}

let initialState = (typeof window !== 'undefined' && window.hasOwnProperty('__initialState__')) ? window.__initialState__ : {}
const store = configureStore(browserHistory, initialState)
const history = syncHistoryWithStore(browserHistory, store)

render(
	<Provider store={store}>
		<Router history={history} routes={routes} store={store} />
	</Provider>,
	document.getElementById('root')
)



// import 'babel-polyfill'
//
// import createLogger from 'redux-logger'
// import thunkMiddleware from 'redux-thunk'
//
// import React from 'react'
// import ReactDOM from 'react-dom'
// import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
// // import { Provider } from 'react-redux'
// import { Router, Route, IndexRoute, browserHistory, NotFoundRoute } from 'react-router'
// import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
// import { ReduxAsyncConnect } from 'redux-async-connect';
//
// import store from './store'
//
// import * as reducers from './reducers'
// import createRootRoute from './routes'
// import { App } from './containers'
// //import { Home, Foo, Bar, Error, NotFound } from './components'
//
// import './scss/index.scss'
//
// // const reducer = combineReducers({
// //     ...reducers,
// //     routing: routerReducer
// // })
// //
// // const store = createStore(
// //     reducer,
// //     compose(
// //         applyMiddleware(thunkMiddleware, createLogger()),
// //         window.devToolsExtension ? window.devToolsExtension() : f => f
// //     )
// // )
//
// const history = syncHistoryWithStore(browserHistory, store)
//
// const component = (
//     <Router history={browserHistory} routes={createRootRoute} store={store} />
// );
//
// ReactDOM.render(
//     <RootRoute store={store} history={history} />,
//     document.getElementById('root')
// )
//
// // ReactDOM.render(
// //     <Provider store={store} key="provider" >
// //         {component}
// //     </Provider>,
// //     document.getElementById('root')
// // )


