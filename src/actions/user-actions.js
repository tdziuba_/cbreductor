/**
 * Created by tdziuba on 2016-05-05.
 */

import * as types from '../constans/user-constants';
import * as common from './common-actions'
import axios from 'axios'

export function requestingLogoutUser(action, state) {
	return {
		type: types.REQ_LOGOUT_USER,
		isLoggedIn: true,
		isFetching: true,
	}
}

export function receivingLogoutUser(user) {
	return {
		type: types.RECV_LOGOUT_USER,
		user
	}
}

export function requestingLoginUser(action, state) {
	return {
		type: types.REQ_LOGIN_USER,
		isLoggedIn: false,
		isFetching: true,
	}
}

export function receivingLoginUser(user) {
	return {
		type: types.RECV_LOGIN_USER,
		user
	}
}

export function logoutUser(dispatch, state) {
	
	dispatch(requestingLogoutUser())
	
	axios({
		url: `http://91.240.238.98:9999/user/logout`,
		timeout: 20000,
		method: 'get',
		responseType: 'json',
		withCredentials: false
	}).then(
		response => {

			let FB = (typeof window.FB !== 'undefined') ? window.FB : null

			FB.getLoginStatus(function (response) {
				if (response.status === 'connected') {
					FB.logout()
				}
			})

			dispatch((response.data.status == 'success') ? receivingLogoutUser(response.data.data, state) : common.receiveError(response.data.message))
		},
		error => dispatch(common.receiveError(error))
	)

}

export function loginUser(dispatch, state) {

	if (state.hasOwnProperty('user') && !state.user.isLoggedIn) {
		dispatch(requestingLoginUser())

		axios({
			url: `http://91.240.238.98:9999/user/login`,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			withCredentials: false
		}).then(
			response => {
				dispatch(
					(response.data.status == 'success') ? receivingLoginUser(response.data.data, state) : common.receiveError(response.data.message)
				)
			},
			error => dispatch(common.receiveError(error))
		)
	} else {
		console.warn('trying to login already logged in user')
	}

}

export function loginByFB(dispatch, state) {

	let FB = (typeof window.FB !== 'undefined') ? window.FB : null

	if (state.hasOwnProperty('user') && !state.user.isLoggedIn) {
		dispatch(requestingLoginUser())

		if (FB) {
			FB.login(function (response) {
				if (response.authResponse) {

					//console.log(response.authResponse)

					FB.api('/me', function (response) {

						let tmp = {
							isLoggedIn: true,
							userName: response.name,
							userAvatar: 'http://static-cdn.citiboard.se/472/img/responsive/avatar_guest_40x40.png',
							FbUserId: response.id,
							isFetching: false
						}

						FB.api('/me/picture', function (resp) {
							tmp.userAvatar = resp.data.url
							dispatch( receivingLoginUser(tmp, state) )
						})

					})
				} else {
					console.log('User cancelled login or did not fully authorize.')
				}
			})
		}

	} else {
		console.warn('trying to login already logged in user')
	}

}