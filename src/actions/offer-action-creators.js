/**
 * Created by tdziuba on 06.04.2016.
 */
import { pushState } from 'redux-react-router';
import * as types from '../constans/offer-constants'
import * as actions from './offer-actions'
import * as common from './common-actions'

export const getAllOffers = common.makeActionCreator(types.REQ_ALL_OFFER)
export const getMoreOffers = common.makeActionCreator(types.REQ_MORE_OFFERS)
export const getOfferDetails = common.makeActionCreator(types.REQ_OFFER)
// export const editTodo = common.makeActionCreator(EDIT_TODO, 'id', 'todo')
// export const removeTodo = common.makeActionCreator(REMOVE_TODO, 'id')