/**
 * Created by tdziuba on 15.03.2016.
 */

import * as types from '../constans/offer-constants.js';
import * as common from './common-actions'

export function requestingOffers(action, state) {
	return {
		type: types.REQ_ALL_OFFER,
		list: [],
		isFetching: true,
	}
}

export function receiveOffers(offers, state) {
	return {
		type: types.RECV_ALL_OFFER,
		list: offers,
		isFetching: false,
	}
}

export function requestingMoreOffers(state) {

	return {
		type: types.REQ_MORE_OFFERS,
		list: state.offers.list,
		isFetching: true,
	}
}

export function receiveMoreOffers(moreOffers, state) {
	return {
		type: types.RECV_MORE_OFFERS,
		list: state.list,
		moreOffers,
		isFetching: false
	}
}

//single offer
export function requestingOfferDetails(state) {
	return {
		type: types.REQ_OFFER,
		offerDetails: {},
		isFetching: true
	}
}

export function receiveOfferDetails(offer) {
	return {
		type: types.RECV_OFFER,
		offerDetails: offer,
		isFetching: false
	}
}

export function addOffer(offer, state) {
	var offers = state.offers.list
	offers.push(offer)

	return {
		type: types.ADD_OFFER,
		list: offers,
		isFetching: false
	};

}

export function deleteOffer(id, offer) {
	return {
		type: types.DELETE_OFFER,
		id,
		offer
	};
}

export function editOffer(id, offer) {
	return {
		type: types.EDIT_OFFER,
		id,
		offer
	};
}