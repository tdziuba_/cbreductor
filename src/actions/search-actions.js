/**
 * Created by tdziuba on 2016-04-25.
 */
import * as types from '../constans/search-constants.js';

export function requestingAutocompleter(action, state) {
	return {
		type: types.REQ_AUTOCOMPLETER,
		autocompleter: [],
		isFetching: true,
	}
}

export function receiveAutocompleterError (error) {
	return {
		type: types.RECV_AUTOCOMPLETER_ERROR,
		error
	}
}

export function receiveAutocompleter(autocompleter, state) {
	return {
		type: types.RECV_AUTOCOMPLETER,
		autocompleter: autocompleter,
		isFetching: false,
	}
}