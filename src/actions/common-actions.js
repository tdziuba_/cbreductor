/**
 * Created by tdziuba on 31.03.2016.
 */

import * as types from '../constans/common-constans.js'
import axios from 'axios'
import { pushState } from 'redux-react-router'


export function makeActionCreator(type, ...argNames) {
	return function(...args) {
		let action = { type }
		argNames.forEach((arg, index) => {
			action[argNames[index]] = args[index]
		})
		return action
	}
}

export function setOverlayWindowEl(overlayWindow) {
	return {
		type: types.SET_OVERLAY,
		overlayWindow
	}
}
export function getOverlayWindowEl(state) {
	return {
		type: types.GET_OVERLAY,
		overlayWindow: state.overlayWindow
	}
}
export function setDialogEl(dialog) {
	return {
		type: types.SET_DIALOG,
		dialog
	}
}
export function getDialogEl(state) {
	return {
		type: types.GET_DIALOG,
		dialog: state.dialog
	}
}
export function setLayoutMode(layoutMode) {
	return {
		type: types.SET_LAYOUT_MODE,
		layoutMode
	}
}
export function getLayoutMode(state) {
	return {
		type: types.GET_LAYUOUT_MODE,
		layoutMode: state.layoutMode
	}
}


export function requestData() {
	return { type: types.REQ_DATA }
}

export function receiveData(json, actionType) {
	return{
		type: actionType,
		data: json
	}
}

export function receiveError(error) {
	return {
		type: types.RECV_ERROR,
		error
	}
}

export function resetErrorMessage() {
	return {
		type: types.RESET_ERROR_MESSAGE
	}
}

export function fetchData(url, actionType) {

	return (dispatch) => {
		dispatch(requestData())
		return {
			type: actionType,
			offers: [{id:1}, {id: 2}]
			// subreddit,
			// posts: json.data.children.map(child => child.data),
			// receivedAt: Date.now()
		}
	}

}