/**
 * Created by tdziuba on 01.04.2016.
 */
import React from 'react'
import {connect} from 'react-redux'
import axios from 'axios'
import * as actions from '../actions/offer-actions'
import { setLayoutMode } from '../actions/common-actions'
import { Spinner, OfferDetailsComponent } from '../components'

class DetailsContainer extends React.Component {
	constructor(props) {
		super(props)
		this.isReady = false
	}

	componentWillMount() {
		this.props.dispatch( setLayoutMode( true ) )
		this.props.parent.forceUpdate()
	}

	componentDidMount() {
		this.fetchOffer()
	}

	fetchOffer() {

		let { dispatch, state, params } = this.props
		let action = actions.requestingOfferDetails(state)

		dispatch(action)

		axios({
			url: `http://91.240.238.98:9999/offer-details?id=${params.id}`,
			timeout: 20000,
			method: 'get',
			responseType: 'json',
			withCredentials: false
		}).then(
			response => {
				dispatch((response.data.status == 'success') ? actions.receiveOfferDetails(response.data.data, state) : actions.receiveOffersError(response.data.message))
			},
			error => dispatch(actions.receiveOffersError(error))
		)
	}
	
	componentDidUpdate() {
		const { state } = this.props
		this.d = state.offers.offerDetails
		this.isReady = !this.isFetched()
	}

	isFetched() {
		const { state } = this.props
		return ( !state.offers.isFetching && state.offers.hasOwnProperty('offerDetails') && state.offers.offerDetails.hasOwnProperty('id') )
	}

	render() {
		const { state } = this.props

		return (
			<div className="offerDetailsContainer">
				{!this.isFetched() ? <Spinner/> : <OfferDetailsComponent data={state.offers.offerDetails} /> }
			</div>
		)
	}
}

function mapStateToProps(state) {
	return { state: state }
}
export default connect(mapStateToProps, null, null, {
	pure: false
})(DetailsContainer)