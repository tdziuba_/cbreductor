/**
 * Created by tdziuba on 15.04.2016.
 */
import React, { Component } from 'react'
import { Router, RouterContext, Link, browserHistory } from 'react-router';
import { connect } from 'react-redux'

import { setLayoutMode } from '../actions/common-actions'
import LoginForm from '../components/login-form/LoginForm'
import RegisterForm from '../components/login-form/RegisterForm'
import ForgottenPassword from '../components/login-form/ForgottenPassword'
import * as actions from '../actions/user-actions'
import { addEvent, removeEvent } from '../helpers/component-helpers'


if (process.browser) {
	require('../components/login-form/login-form.scss') //style
}

class AccessForm extends Component {

	constructor(props) {
		super(props)
		this.action = 'login'

		this.user = this.props.state.user
		this.loginBtn = null
		this.registerBtn = null
	}
	
	componentWillMount() {
		let { dispatch, state } = this.props

		dispatch( setLayoutMode( true ) )

		if (this.action == 'logout' && state.user.isLoggedIn) {
			actions.logoutUser(dispatch, state)
		}

		this.props.parent.forceUpdate()

	}

	componentDidMount() {
		this.handleFormEvents()
	}

	componentDidUpdate() {
		let { state, history } = this.props

		if (state.user.isLoggedIn !== this.user.isLoggedIn) {
			history.goBack()
		}

	}

	setAction(action) {
		const { state, dispatch, history } = this.props
		let oldAction = this.action

		this.action = action

		if (action == 'logout') {
			actions.logoutUser(dispatch, state)
			history.push('/')
		} else if (this.action !== oldAction) {
			this.forceUpdate()
		}

	}

	handleFormEvents() {
		let self = this

		browserHistory.listen(function (ev) {
			let path = ev.pathname + ''

			if (path.indexOf('/access/logout') == 0) {
				self.setAction('logout')
			} else if (path.indexOf('/access/register') == 0) {
				self.setAction('register')
			} else if (path.indexOf('/access/forgotten-password') == 0) {
				self.setAction('forgotten-password')
			} else if (path.indexOf('/access/login') == 0) {
				self.setAction('login')
			}

		})

		// addEvent(self.refs.loginBtn, 'click', self.displayLoginForm.bind(this))
		// addEvent(self.refs.registerBtn, 'click', self.displayRegisterForm.bind(this))

	}

	componentWillUnmount() {
		// removeEvent(this.refs.loginBtn, 'click', this.displayLoginForm)
		// removeEvent(this.refs.registerBtn, 'click', this.displayRegisterForm)
	}

	displayLoginForm() {
		this.setAction('login')
		this.forceUpdate()
	}

	displayRegisterForm() {
		this.setAction('register')
		this.forceUpdate()
	}

	displayForgottenForm() {
		this.setAction('forgotten-password')
		this.forceUpdate()
	}

	getForm() {

		if (this.action === 'forgotten-password') {
			return (<ForgottenPassword parent={this} ref="forgottenForm" />)
		} else if (this.action === 'register') {
			return (<RegisterForm parent={this} ref="registerForm" />)
		} else if (this.action === 'logout') {
			return (<p className="centered">You are logged out. Redirecting...</p>)
		} else {
			return (<LoginForm parent={this} ref="loginForm" />)
		}

	}

	loginByFb() {
		let { dispatch, state } = this.props
		actions.loginByFB(dispatch, state)
	}

	submitLoginForm(email, password) {
		if (password.length > 4) {
			alert('fields are valid - we\'re sending form with ' + email + ' and ' + password + '...')
			browserHistory.push('/')
		} else {
			alert('randomly for tests - fields are INVALID - we\'re showing errors')
			this.refs.loginForm.errors = {
				pass: 'PLEASE INSERT VALID PASSWORD:',
				email: 'PLEASE INSERT VALID E-MAIL:'
			}
			this.refs.loginForm.forceUpdate()
		}
	}

	submitForgottenForm(email) {
		alert('here will be ajax for sending email to api')
		this.refs.forgottenForm.showSuccess(true).forceUpdate()
	}

	render() {
		let action = this.action,
			navClass = (action == 'forgotten-password' || action == 'logout') ? ' hidden' : '',
			registerBtnClass = (action == 'register') ? ' active' : '',
			loginBtnClass = (action == 'login') ? ' active' : ''

		return(
			<div className="accessFormWrapper">
				<nav className={`accessFormNav${navClass}`}>
					<Link className={`btn btn-large showLoginFormBtn${loginBtnClass} `} ref="loginBtn" to="/access/login">Login</Link>
					<Link className={`btn btn-large showRegisterFormBtn${registerBtnClass} `} ref="registerBtn" to="/access/register">Register</Link>
				</nav>
				<div className="mainFormWrapper">
					{ this.getForm() }
				</div>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return { state: state, user: state.user }
}
export default connect(mapStateToProps)(AccessForm)
