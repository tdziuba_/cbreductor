/**
 * Created by tdziuba on 15.03.2016.
 */

import React, {Component} from 'react';
import { Router, RouterContext, Link, browserHistory } from 'react-router';
import {connect} from 'react-redux';
import _ from 'underscore'

import FullLayout from '../components/layout/FullLayout'
import NoLayout from '../components/layout/NoLayout'

import { addEvent, removeEvent } from '../helpers/component-helpers'
import * as actions from '../actions/common-actions'
import { resetErrorMessage } from '../actions'
import { store } from '../store'


//connect(state => ({routerState: state.router, offers: state.offers }))

class App extends Component {
	constructor(props) {
		super(props);

		this.overlayWindow = null

		this.handleChange = this.handleChange.bind(this)
		this.handleDismissClick = this.handleDismissClick.bind(this)
		this.setLayoutMode()
	}

	handleDismissClick(e) {
		this.props.resetErrorMessage()
		e.preventDefault()
	}

	handleChange(nextValue) {
		browserHistory.push(`/${nextValue}`)
	}

	setLayoutMode() {
		let {location, dispatch} = this.props
		let noLayoutModePaths = ['/login', '/bloggse', '/youtube', '/landing']
		let list =  _.filter(noLayoutModePaths, function(path) {
			return (typeof window !== 'undefined' && window.location.pathname !== '/' && window.location.pathname.indexOf(path) > -1)
		})

		let layoutMode = (list.length == 0) //nie ma na liście wykluczeń - więc jest

		dispatch( actions.setLayoutMode( layoutMode ) )
	}

	componentDidMount() {
		let { dispatch } = this.props
		dispatch( actions.setOverlayWindowEl(this.overlayWindow) )
		
		this.initFB()
	}

	initFB() {
		window.fbAsyncInit = function() {
			FB.init({
				appId      : '238480503186064',
				xfbml      : true,
				version    : 'v2.6'
			});
		};

		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/sv_SE/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}

	render() {
		const { children, state, dispatch } = this.props
		let layoutMode = state.commons.layoutMode

		return ( layoutMode ? <FullLayout parent={this} dispatch={dispatch} router={this.context.router} state={state}>{children}</FullLayout> : <NoLayout parent={this} router={this.context.router} state={state}>{children}</NoLayout> )

	}
}

function mapStateToProps(state) {
	return {
		state: state,
		routerState: state.router,
		offers: state.offers,
		layoutMode: (state.hasOwnProperty('layoutMode') ? state.layoutMode : true),
		...state
	}
}
export default connect(mapStateToProps, null, null, {
	pure: false
})(App)

// import React from 'react'
// import { Link, browserHistory } from 'react-router'
// import Feed from './FeedContainer'
// import Header from '../components/header/HeaderComponent.js'
//
// export default function App({ children }) {
// 	return (
// 		<div>
// 			<Header/>
// 			<div>
// 				<button onClick={() => browserHistory.push('/foo')}>Go to /foo</button>
// 			</div>
// 			<div style={{ marginTop: '1.5em' }}>{children || <Feed/>}</div>
// 		</div>
// 	)
// }