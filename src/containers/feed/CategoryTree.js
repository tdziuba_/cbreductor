/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-02
 */

import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FeedControls from './FeedControls'

class CategoryTree extends FeedControls {
	constructor(props) {
		super(props)
		
		this.setTitle('Categories')
		this.setChildren(
			(
				<nav className="categoryList">
					<Link to="/" className="categoryListItem"><i className="icon icon-car" aria-hidden="true"/><span>Cars</span></Link>
					<Link to="/" className="categoryListItem"><i className="icon icon-desktop" aria-hidden="true"/><span>Electrinics</span></Link>
					<Link to="/" className="categoryListItem"><i className="icon icon-shoe" aria-hidden="true"/><span>Fashion &amp; Accessories</span></Link>
					<Link to="/" className="categoryListItem"><i className="icon icon-sofa" aria-hidden="true"/><span>Home &amp; Garden</span></Link>
					<Link to="/" className="categoryListItem"><i className="icon icon-houses" aria-hidden="true"/><span>Real Estate</span></Link>
					<Link to="/" className="categoryListItem"><i className="icon icon-football" aria-hidden="true"/><span>Sport &amp; Leisure</span></Link>
					<Link to="/" className="categoryListItem"><i className="icon icon-network" aria-hidden="true"/><span>Others</span></Link>
				</nav>
			)
		)
	}
}

function mapStateToProps(state) {
	return {
		state: state,
		...state
	}
}

export default connect(mapStateToProps)(CategoryTree)