/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-02
 */

import React, {Component} from 'react'
import {connect} from 'react-redux'
import { addEvent, removeEvent } from '../../helpers/component-helpers'

class Filters extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		let self = this

		addEvent(self.refs.priceFilterForm, 'submit', self.handleFilteringPrice.bind(this))
		addEvent(self.refs.filterNewest, 'click', self.handleSorting.bind(this))
		addEvent(self.refs.filterPriceAsc, 'click', self.handleSorting.bind(this))
		addEvent(self.refs.filterPriceDesc, 'click', self.handleSorting.bind(this))
	}

	componentWillUnmount() {
		let self = this

		removeEvent(self.refs.priceFilterForm, 'submit', self.handleFilteringPrice)
		removeEvent(self.refs.filterNewest, 'click', self.handleSorting)
		removeEvent(self.refs.filterPriceAsc, 'click', self.handleSorting)
		removeEvent(self.refs.filterPriceDesc, 'click', self.handleSorting)
	}

	handleFilteringPrice(ev) {
		ev.preventDefault()

		this.props.parent.filterResults( parseInt(this.refs.priceFrom.value), parseInt(this.refs.priceTo.value) )
	}

	handleSorting(ev) {
		this.props.parent.sortResults(ev.target.dataset.sort)

		this.refs.filterPriceAsc.className = this.refs.filterPriceAsc.className.replace('checked', '')
		this.refs.filterPriceDesc.className = this.refs.filterPriceDesc.className.replace('checked', '')
		this.refs.filterNewest.className = this.refs.filterNewest.className.replace('checked', '')

		ev.target.className = ev.target.className + ' checked'
	}

	render() {

		return (
			<section className="feedControls categoryFilters">
				<h3 className="feedControlsHeader">Filter offers</h3>
				<form className="form-inline row" ref="priceFilterForm">
					<label className="col col-9of16 col-phablet-160f16"><span>Price from:</span><input type="text" className="input input-medium" ref="priceFrom" /></label>
					<label className="col col-7of16 col-phablet-160f16"><span>to:</span><input type="text" className="input input-medium" ref="priceTo" /></label>
					<input type="submit" className="col col16of16 hidden" />
				</form>
				<h3 className="feedControlsHeader inside">Sort offers</h3>
				<ul>
					<li className="categoryListItem" ref="filterPriceAsc" data-sort="price_asc">Price ascending<i className="fa fa-check" aria-hidden="true" /></li>
					<li className="categoryListItem" ref="filterPriceDesc" data-sort="price_desc">Price descending<i className="fa fa-check" aria-hidden="true" /></li>
					<li className="categoryListItem" ref="filterNewest" data-sort="newest">Newest<i className="fa fa-check" aria-hidden="true" /></li>
				</ul>
			</section>
		)
	}
}

function mapStateToProps(state) {
	return {
		state: state,
		...state
	}
}

export default connect(mapStateToProps)(Filters)