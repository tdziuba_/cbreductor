/**
 * Created by tdziuba on 01.04.2016.
 */
import React from 'react'
import {connect} from 'react-redux'
import axios from 'axios'

import * as actions from '../../actions/offer-actions'
import { setLayoutMode, receiveError } from '../../actions/common-actions'

import MobileAppsBanner from '../../components/common/MobileAppsBanner'
import FeedControls from './FeedControls'
import CategoryTree from './CategoryTree'
import TopLocations from './TopLocations'
import Filters from './Filters'
import OfferList from '../../components/offers/OfferList'
import OfferGridItem from '../../components/offers/OfferGridItem'
import AppendOffersButton from '../../components/offers/AppendOffersButton'
import Spinner from '../../components/spinner/Spinner'

if (process.browser) {
	require('./category-tree.scss');
}

class FeedContainer extends React.Component {
	constructor(props) {
		super(props);
		this.limit = 60
		this.offset = 0
		this.filters = {
			min: 0,
			max: 100000000
		}
		this.sort = 'newest'
		this.offers = []
		this.offerList = null
	}

	componentWillMount() {
		this.props.dispatch( setLayoutMode( true ) )
		this.props.parent.forceUpdate()
	}

	componentDidMount () {
		this.fetchOffers();
	}

	fetchOffers() {

		let { dispatch, state } = this.props
		let action = (this.offset === 0) ? actions.requestingOffers(state) : actions.requestingMoreOffers(state)
		let self = this

		dispatch(action)

		// axios({
		// 	url: 'http://91.240.238.98:9999/offers',
		// 	timeout: 20000,
		// 	method: 'get',
		// 	responseType: 'json',
		// 	withCredentials: false,
		// 	params: {
		// 		offset: self.offset,
		// 		min: self.filters.min,
		// 		max: self.filters.max,
		// 		sort: self.sort
		// 	}
		// }).then(
		// 	response => {
		// 		if (self.offset === 0) {
		// 			dispatch((response.data.status == 'success') ? actions.receiveOffers(response.data.data.results_array, state) : actions.receiveOffersError(response.data.message))
		// 		} else {
		// 			dispatch((response.data.status == 'success') ? actions.receiveMoreOffers(response.data.data.results_array, state) : actions.receiveOffersError(response.data.message))
		// 		}
		//
		// 		self.appendOffersToList()
		// 	},
		// 	error => dispatch(receiveError(error))
		// )


	}

	componentDidUpdate() {

	}

	appendOffersToList() {
		const { state } = this.props
		const self = this

		if (state.offers.list && state.offers.list.length && typeof state.offers.moreOffers === 'undefined') {
			state.offers.list.map(function (offer) {
				self.offers.push( <OfferGridItem data={offer} key={offer.id}/> )
			});

			this.forceUpdate()
		}

		if (state.offers.moreOffers && state.offers.moreOffers.length) {
			state.offers.moreOffers.map(function (offer) {
				self.offers.push( <OfferGridItem data={offer} key={offer.id}/> )
			});

			this.forceUpdate()
		}

	}

	incrementOffset() {
		this.offset += this.limit
	}

	appendMore() {
		this.incrementOffset()
		this.fetchOffers()
	}

	filterResults(from, to) {
		if ((from && typeof from === 'number') || (to && typeof to === 'number')) {
			this.offset = 0
			this.filters = {
				min: parseInt(from),
				max: parseInt(to)
			}

			this.offers = []

			this.fetchOffers()
		}
	}

	sortResults(type) {
		this.sort = type;

		this.offers = []

		this.fetchOffers()
	}

	expandActiveControl(component) {
		this.refs.CategoryTree.setInactive()
		this.refs.TopLocations.setInactive()
		component.setActive()
		component.forceUpdate()
	}

	render() {
		let { isFetching } = this.props

		return (
			<div className="row vtop feed">
				<MobileAppsBanner />
				<OfferList children={[]} ref={(c) => this.offerList = c}>
					{ this.offers }
					{ isFetching ? <Spinner/> : '' }
				</OfferList>
				<div className="col col-4of16 cbSidebar">
					<CategoryTree ref="CategoryTree" parent={this} />
					<TopLocations ref="TopLocations" parent={this} />
					<Filters parent={this} />
				</div>

				<div className="col col-16of16">
					<AppendOffersButton isFetching={ isFetching } parent={ this } />
				</div>

			</div>
		)
	}
}

function mapStateToProps(state) {
	return { state: state, offers: state.offers.list, isFetching: state.offers.isFetching }
}
export default connect(mapStateToProps, null, null, {
	pure: false
})(FeedContainer)

