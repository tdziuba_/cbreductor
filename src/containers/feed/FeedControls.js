/**
 * Created by Citiboard AB Team
 * User: tdziuba <tomasz.dziuba@sprzedajemy.pl>
 * Date: 27.05.2016  08:30
 */

import React, {Component} from 'react'
import { addEvent, removeEvent } from '../../helpers'


class FeedControls extends Component {
	constructor(props) {
		super(props)

		this.parent = this.props.parent
		this.isActive = false;
		this.title = ''
		this.children = null
	}

	componentDidMount() {
		console.log(this.refs.header)
		addEvent( this.refs.header, 'click', this.handleHeaderClick.bind(this) )
	}

	componentWillUnmount() {
		removeEvent( this.refs.header, 'click', this.handleHeaderClick )
	}
	
	setActive() {
		this.isActive = true;
	}

	setInactive() {
		this.isActive = false;
	}

	handleHeaderClick(ev) {
		console.log(ev)
		this.parent.expandActiveControl(this)
	}
	
	setChildren(children) {
		this.children = children
	}

	setTitle(title) {
		this.title = title
	}
	
	renderWrapper() {
		return (
			<section className={`feedControls categoryTree ${ this.isActive ? 'active' : '' }`} ref="feedControl">
				{ this.renderHeader() }
				{ this.children ? this.children : '' }
			</section>
		)
	}
	
	renderHeader() {
		return (
			<h3 className="feedControlsHeader" ref="header">{ this.title }<i className={`icon icon-angle-${ this.isActive ? 'up' : 'down' }`} aria-hidden="true" ref="icon"/></h3>
		)
	}

	render() {

		return this.renderWrapper()
	}
}

export default FeedControls
