/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-04
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { addEvent, removeEvent } from '../../helpers'
import FeedControls from './FeedControls'

class TopLocations extends FeedControls {
	constructor(props) {
		super(props)

		this.setTitle('Locations')
		this.setChildren(
			(
				<nav className="categoryList">
					<Link to="/" className="categoryListItem"><span>All Sweden</span></Link>
					<Link to="/" className="categoryListItem"><span>Stockholm</span></Link>
					<Link to="/" className="categoryListItem"><span>Gothenburg</span></Link>
					<Link to="/" className="categoryListItem"><span>Malmö</span></Link>
					<Link to="/" className="categoryListItem"><span>Uppsala</span></Link>
					<Link to="/" className="categoryListItem"><span>Halmstad</span></Link>
					<div className="categoryListItem"><button className="btn moreBtn">more...</button></div>
				</nav>
			)
		)
	}

}

function mapStateToProps(state) {
	return {
		state: state,
		...state
	}
}

export default connect(mapStateToProps)(TopLocations)