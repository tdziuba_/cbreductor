/**
 * Created by tdziuba on 01.04.2016.
 */
import React from 'react'
import { getAllOffers } from '../actions/offer-actions.js'
import { setLayoutMode } from '../actions/common-actions'

class FavouritesContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this.props.dispatch( setLayoutMode( true ) )
		this.props.parent.forceUpdate()
	}

	fetchOffers() {
		getAllOffers()
	}

	render() {
		return (
			<div className="feed">
				<h2>Lista ulubionych ofert</h2>
			</div>
		)
	}
}

module.exports = FavouritesContainer