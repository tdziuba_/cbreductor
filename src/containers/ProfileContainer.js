/**
 * Created by tdziuba on 01.04.2016.
 */
import React from 'react'
import { getAllOffers } from '../actions/offer-actions.js'

class ProfileContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	onEnter () {
		this.fetchOffers();
	}

	fetchOffers() {
		console.log('fetchOffers');
		dispatch(getAllOffers())
	}

	render() {
		return (
			<div className="userProfile">
				<h2>Profile użytkownika</h2>
			</div>
		)
	}
}

module.exports = ProfileContainer