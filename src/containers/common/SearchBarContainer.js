/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-25
 */

import React, {Component} from 'react'
import {connect} from 'react-redux'
import axios from 'axios'
import * as actions from '../../actions/search-actions'
import SearchBar from '../../components/header/SearchBar'

class SearchBarContainer extends Component {
	constructor(props) {
		super(props)
		this.searchBar = null
	}
	
	componentDidMount() {
		
	}

	fetchAutocompleter(phrase) {
		const self = this
		const { dispatch, state } = this.props

		dispatch(actions.requestingAutocompleter(state))

		axios({
			url: 'http://91.240.238.98:9999/autocompleter?q=' + phrase,
			timeout: 20000,
			method: 'post',
			responseType: 'json',
			withCredentials: false
		}).then(
			response => {
				dispatch((response.data.status == 'success') ? actions.receiveAutocompleter(response.data.data.results, state) : actions.receiveAutocompleterError(response.data.message))

				self.searchBar.showAutocompleter(response.data.data.results)
			},
			error => dispatch( actions.receiveAutocompleterError(error) )
		)
	}

	render() {
		const {children} = this.props

		return (
			<SearchBar parent={this} ref={(ref) => this.searchBar = ref}>
				{ children }
			</SearchBar>
		)
	}
}

function mapStateToProps(state) {
	return {
		state: state,
		...state
	}
}

export default connect(mapStateToProps)(SearchBarContainer)