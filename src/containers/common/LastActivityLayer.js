/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-04-29
 */

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { addEvent, removeEvent } from '../../helpers/component-helpers'

class LastActivityLayer extends Component {
	constructor(props) {
		super(props)
		this.layer = null
		this.list = null
		this.amount = 0
	}

	componentDidMount() {
		addEvent(ReactDOM.findDOMNode(this.layer), 'click', this.expandActivityList);
	}

	componentWillUnmount() {
		removeEvent(ReactDOM.findDOMNode(this.layer), 'click', this.expandActivityList);
	}

	expandActivityList(ev) {
		this.refs.list.className = this.refs.list.className.replace('hidden', '')
	}

	setCounter(amount) {
		this.amount = amount
		this.forceUpdate()
	}

	getCounter() {
		return (
			<em className="counter" aria-hidden="true">{ this.amount }</em>
		)
	}

	getButton() {

		return (
			<button className="btn-text counterWrp">
				<i className="icon icon-chat" aria-hidden="true" />
				{ this.getCounter() }
			</button>
		)
	}

	getLink() {

		return (
			<Link to="/messages" className="btn-text counterWrp">
				<i className="icon icon-chat" aria-hidden="true" />
			</Link>
		)
	}

	render() {
		const {children} = this.props

		return (
			<div className="lastActivityLayer" ref="activityBtn" >
				{ (this.amount > 0) ? this.getButton() : this.getLink() }
				<ul className="lastActivityList hidden" ref="list" >
					<li>info</li>
				</ul>
				<div className="layer" ref="layer" />
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		state: state,
		...state
	}
}

export default connect(mapStateToProps)(LastActivityLayer)