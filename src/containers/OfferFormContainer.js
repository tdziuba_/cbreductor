/**
 * Created by Citiboard AB Team.
 * User: tdziuba
 * Date: 2016-05-06
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import OfferFormPictures from '../components/offer-form/OfferFormPictures'
import OfferFormForm from '../components/offer-form/OfferFormForm'
import OfferFormLocation from '../components/offer-form/OfferFormLocation'

if (process.browser) {
	require('../components/offer-form/offer-form.scss')
}

class OfferForm extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {

	}

	render() {
		const { user } = this.props

		return (
			<div className="container">
				<div className="offerFormWrapper popup">
					<div className="popupHeader">
						<h3>Add an offer for <span className="label label-primary label-large">free!</span></h3>
					</div>
					<OfferFormPictures />
					<OfferFormForm />
					<div className="popupFooter">
						<button className="btn btn-large btn-secondary addOfferBtn">Sell your item &raquo;</button>
					</div>
				</div>
			</div>
		)
	}
}

function mapStateToProps(state) {
	return {
		state: state,
		...state
	}
}

export default connect(mapStateToProps)(OfferForm)