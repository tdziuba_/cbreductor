/**
 * Created by tdziuba on 15.04.2016.
 */
import * as types from '../constans/common-constans'

const initialState = {
	overlayWindow: null,
	dialog: null,
	tooltip: null
}

export default function popups(state = initialState, action) {

	if(action.type === types.SET_OVERLAY) {
		return { overlayWindow: action.overlayWindow }
	}
	else if(action.type === types.GET_OVERLAY) {
		return { overlayWindow: state.overlayWindow }
	}
	else if(action.type === types.SET_DIALOG) {
		return { dialog: action.dialog }
	}
	else if(action.type === types.GET_DIALOG) {
		return { dialog: state.dialog }
	}

	return state
}