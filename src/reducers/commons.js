/**
 * Created by tdziuba on 2016-04-19.
 */
import * as types from '../constans/common-constans'

const initialState = {
	layoutMode: true,
	
}

export default function commons(state = initialState, action) {

	if(action.type === types.GET_LAYOUT_MODE) {
		return { layoutMode: state.layoutMode }
	}
	else if(action.type === types.SET_LAYOUT_MODE) {
		return { layoutMode: action.layoutMode }
	}

	return state
}