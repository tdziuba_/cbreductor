/**
 * Created by tdziuba on 15.03.2016.
 */
import * as types from '../constans/offer-constants.js'
import _ from 'underscore'

const initialState = {
	list: [],
	offerDetails: {},
	isFetching: false
}

export default function offers(state = initialState, action) {

	if(action.type === types.RECV_ALL_OFFER) {
		return { list: action.list, isFetching: action.isFetching }
	}
	else if(action.type === types.RECV_MORE_OFFERS) {
		return { list: state.list, moreOffers: action.moreOffers, isFetching: action.isFetching }
	}
	else if(action.type === types.REQ_ALL_OFFER) {
		return { list: action.list, isFetching: true }
	}
	else if(action.type === types.REQ_MORE_OFFERS) {
		return { list: state.list, isFetching: true }
	}
	else if (action.type === types.REQ_OFFER) {
		return { offerDetails: {}, isFetching: true }
	}
	else if (action.type === types.RECV_OFFER) {
		return { offerDetails: action.offerDetails, isFetching: action.isFetching }
	}

	return state
}

// export default function offerDetails(state = {offer: {}, isFetching: false}, action) {
//
//
// 	if(action.type === types.REQ_OFFER) {
// 		return { offer: action.offer, isFetching: action.isFetching }
// 	}
// 	else if(action.type === types.RECV_OFFER) {
// 		return { offer: action.offer, isFetching: action.isFetching }
// 	}
// 	else if(action.type === types.ADD_OFFER) {
// 		return { offer: action.offer, isFetching: action.isFetching }
// 	}
// 	else if(action.type === types.EDIT_OFFER) {
// 		return action.offer
// 	}
//
// 	return state
// }
