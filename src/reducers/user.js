/**
 * Created by tdziuba on 2016-04-19.
 */
import * as types from '../constans/user-constants'

const initialState = {
	isLoggedIn: false,
	userName: '',
	userAvatar: 'http://static-cdn.citiboard.se/472/img/responsive/avatar_guest_40x40.png',
	FbUserId: 0,
	isFetching: false
}

export default function user(state = initialState, action) {

	if(action.type === types.REQ_LOGOUT_USER) {
		return { isFetching: true, isLoggedIn: true }
	}
	else if(action.type === types.RECV_LOGOUT_USER) {
		return initialState;
	}
	else if(action.type === types.REQ_LOGIN_USER) {
		return {
			isFetching: true,
			isLoggedIn: false
		}
	}
	else if(action.type === types.RECV_LOGIN_USER) {

		return {
			isFetching: true,
			isLoggedIn: true,
			userName:action.user.userName ,
			userAvatar: action.user.userAvatar,
			FbUserId: action.user.FbUserId,
		}
	}

	return state
}