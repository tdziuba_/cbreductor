/**
 * Created by tdziuba on 15.04.2016.
 */
import * as types from '../constans/search-constants'

const initialState = {
	autocompleter: null,
	searchedOffers: null,
	isFetching: false
}

export default function popups(state = initialState, action) {

	if(action.type === types.REQ_AUTOCOMPLETER) {
		return { autocompleter: action.autocompleter, isFetching: action.isFetching }
	}
	else if(action.type === types.RECV_AUTOCOMPLETER) {
		return { autocompleter: action.autocompleter, isFetching: action.isFetching }
	}

	return state
}