/**
 * Created by tdziuba on 06.04.2016.
 */
import { RECV_ERROR, RESET_ERROR_MESSAGE } from '../constans/common-constans'

export default function errorMessage(state = null, action) {
	const { type, error } = action

	if (type === RESET_ERROR_MESSAGE) {
		return null
	} else if (type == RECV_ERROR) {
		return action.error.message
		
	} else if (error) {
		return action.error.message
	}

	return state
}