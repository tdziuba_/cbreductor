# INSTALACJA

* Instalacja nodejs wraz z npm
* Instalacja globalna komponentów:

`npm install -g mocha babel-cli`

* Instalacja komponentów lokalnych z package.json

`npm install`

## Produkcja

Aby przygotować kod js i css do wrzutki na produkcję należy uruchomić poniższą komendę:

`npm run production`

W katalogu `public` pojawią się pliki które powinny być wrzucone na produkcję

## Server developerski

Konfiguracja umożliwia odpalenie 2 typów środowiska deweloperskiego.
Pierwszy to tak zwany hot module replacement - podmienia kod js i css w locie, bez tworzenia plików fizycznie, przez co jest szybki i nie wymaga uploadu
Drugi to klasyczne budowanie plików (całkiem szybkie) ale wymaga wrzutki na serwer developerski, co może zająć cenne ułamki sekund

Aby dopalić wariant `hot module replacement` należy:

`npm run start`

Aby odpalić wariant tradycyjny z uploadem plików należy wykonać poniższą komendę:

`npm run dev-watch`

## Testy

Aby odpalić testy jednostkowe dla js'ów należy wykonać poniższy kod:

`npm run test`

## PhpStorm

W PhpStorm twórcy programu umieścili konsolę npm, dzięki której nie trzeba wpisywać większości komend ręcznie. Należy jednynie wpisać (o lie nie mamy już projektu zainstalowanego):

`npm install -g mocha babel-cli`

oraz

`npm install`

Następnie wystarczy kliknąć prawym klawiszem myszy na pliku package.json i wybrać show npm scripts. Pojawi się konsola npm z możliwością odpalania konkretnych zadań