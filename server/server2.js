/*eslint-disable no-console */
import express from 'express'
import serialize from 'serialize-javascript'

import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackConfig from '../webpack.config'

import React from 'react'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import { createMemoryHistory, match, RouterContext } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import { configureStore } from '../src/store'
import routes from '../src/routes'

const port = 3000
const app = express()

let compiler = webpack(webpackConfig)

app.use(webpackDevMiddleware(compiler, {
	noInfo: false,
	publicPath: webpackConfig.output.publicPath,
	reload: true,
	hot: true,
	watchOptions: {
		aggregateTimeout: 300,
		poll: 1000
	},
	timeout: 300,
	stats: {colors: true},
	historyApiFallback: true
}))

app.use(require("webpack-hot-middleware")(compiler, {
	log: console.log, path: '/__webpack_hmr', heartbeat: 10 * 1000
}));

app.use('/static', express.static('public'))

const HTML = ({ content, store }) => (
	<html className="no-js">
	<head>
		<meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Citiboard.se - Sveriges största loppis i mobilen</title>
		<link href="/static/css/main.css" type="text/css" rel="stylesheet" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" />
	</head>
	<body>
	<div id="root" dangerouslySetInnerHTML={{ __html: content }} />
	<div id="popups"/>
	<script dangerouslySetInnerHTML={{ __html: `window.__initialState__=${serialize(store.getState())};` }} />
	<script src="/static/js/main.js" />
	</body>
	</html>
)

app.use(function (req, res) {

	const memoryHistory = createMemoryHistory(req.url)
	const store = configureStore(memoryHistory)
	const history = syncHistoryWithStore(memoryHistory, store)

	match({ history, routes, location: req.url }, (error, redirectLocation, renderProps) => {
		if (error) {
			res.status(500).send(error.message)
		} else if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
		} else if (renderProps) {
			const content = renderToString(
				<Provider store={store}>
					<RouterContext {...renderProps}/>
				</Provider>
			)

			res.header("Content-Type", "text/html");
			res.send('<!doctype html>\n' + renderToString(<HTML content={content} store={store}/>))
		}
	})
})

app.listen(port, function () {
	console.log('Server listening on http://localhost:%s, Ctrl+C to stop', port)
})