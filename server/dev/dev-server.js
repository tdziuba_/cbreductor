/**
 * Created by tdziuba on 2016-05-19.
 */
import path from 'path'
import fs from 'fs'
import express from 'express'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { Router, createMemoryHistory, match, RouterContext } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { configureStore } from '../../src/store'
import routes from '../../src/routes'

const app = express()
const port = 3000

// This is fired every time the server side receives a request
app.disable('etag');
app.use( express.static( path.resolve(__dirname, '../../public') ) )
app.use(handleRender)

// We are going to fill these out in the sections to follow
function handleRender(req, res) {

	// Create a new Redux store instance
	const _initialState = {}
	const memoryHistory = createMemoryHistory(req.url)
	const store = configureStore(memoryHistory, _initialState)
	const history = syncHistoryWithStore(memoryHistory, store)

	// Render the component to a string
	const content = renderToString(
		<Provider store={store} history={history} routes={routes} store={store}>
			<Router history={history} routes={routes} store={store} />
		</Provider>
	)

	// Grab the initial state from our Redux store
	const initialState = store.getState()

	// Send the rendered page back to the client
	res.send(renderFullPage(content, initialState))
}

function renderFullPage(content, initialState) {
	return `
    <!doctype html>
    <html>
      <head>
      	<meta charSet="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Citiboard.se - Sveriges största loppis i mobilen</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,500,700"  type="text/css" rel="stylesheet"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"  type="text/css" rel="stylesheet"/>
		<link href="/css/index.css" type="text/css" rel="stylesheet"/>
      </head>
      <body>
        <div id="root">${content}</div>
        <script>
          window.__initial_state__ = ${JSON.stringify(initialState)}
        </script>
        <script src="/js/common.js"></script>
        <script src="/js/index.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
      </body>
    </html>
    `
}

app.listen(port, (error) => {
	if (error) {
		console.error(error)
	} else {
		console.info(`==> 🌎  Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`)
	}
})
