import express from 'express'
import fs from 'fs'
import path from 'path'
import serialize from 'serialize-javascript'
import helmet from 'helmet'

import React from 'react'
import ReactDOMServer from 'react/lib/ReactDOMServer'
//import { renderToString, renderToStaticMarkup } from 'react-dom/server'
import { Provider } from 'react-redux'
import { Router, createMemoryHistory, match, RouterContext } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import { configureStore } from '../../src/store'
import routes from '../../src/routes'
import App from '../../src'

const port = 3000
const app = express()
let DOM = React.DOM, body = DOM.body, div = DOM.div, script = DOM.script

app.use(helmet())
app.use( express.static( path.resolve(__dirname, '../../public') ) )


app.use(function (req, res) {
	const memoryHistory = createMemoryHistory(req.url)
	const store = configureStore(memoryHistory)
	const history = syncHistoryWithStore(memoryHistory, store)

	if (store) {

		let html = ReactDOMServer.renderToString(
			<head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				<title>Citiboard.se - Sveriges största loppis i mobilen</title>
				<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"  type="text/css" rel="stylesheet"/>
				<link href="/css/index.css" type="text/css" rel="stylesheet" />
				<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" />
			</head>
		)

		html += ReactDOMServer.renderToStaticMarkup( body( null,

			// The actual server-side rendering of our component occurs here, and we
			// pass our data in as `props`. This div is the same one that the client
			// will "render" into on the browser from browser.js
			div({
				id: 'root', dangerouslySetInnerHTML: {
					__html: ReactDOMServer.renderToString(
						<Provider store={store} history={history} routes={routes} store={store}>
							<Router history={history} routes={routes} store={store} />
						</Provider>
					)
				}
			}),

			// The props should match on the client and server, so we stringify them
			// on the page to be available for access by the code run in browser.js
			// You could use any var name here as long as it's unique


			// script from body bottom
			script({ src: '/js/common.js' }),
			script({ src: '/js/index.js' }),
			script({ src: '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js' }),

			script({
				dangerouslySetInnerHTML: {
					__html: `window.__initialState__=${serialize(store.getState())};`
				}
			}),


		))

		// Return the page to the browser
		res.header('Content-Type', 'text/html')
		res.send('<!doctype html><html className="no-js">\n' + html + '</html>')
	}
})

// app.all('/css/*', function (req, res, next) {
// 	res.header('Content-Type', 'text/css')
// 	res.sendFile('..')
// })



// const HTML = ({ content, store }) => (
// 	<html className="no-js">
// 	<head>
// 		<meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
// 		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
// 		<title>Citiboard.se - Sveriges största loppis i mobilen</title>
// 		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"  type="text/css" rel="stylesheet"/>
// 		<link href="/css/common.css" type="text/css" rel="stylesheet" />
// 		<link href="/css/index.css" type="text/css" rel="stylesheet" />
// 		<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" />
// 	</head>
// 	<body>
// 	<div id="root" dangerouslySetInnerHTML={{ __html: content }} />
// 	<div id="popups"/>
// 	<script dangerouslySetInnerHTML={{ __html: `window.__initialState__=${serialize(store.getState())};` }} />
// 	<script src="/js/common.js" />
// 	<script src="/js/index.js" />
//
// 	</body>
// 	</html>
// )
//
// app.use(function (req, res) {
//
// 	const memoryHistory = createMemoryHistory(req.url)
// 	const store = configureStore(memoryHistory)
// 	const history = syncHistoryWithStore(memoryHistory, store)
//
// 	match({ history, routes, location: req.url }, (error, redirectLocation, renderProps) => {
// 		if (error) {
// 			res.status(500).send(error.message)
// 		} else if (redirectLocation) {
// 			res.redirect(302, redirectLocation.pathname + redirectLocation.search)
// 		} else if (renderProps) {
// 			const content = renderToString(
// 				<Provider store={store} history={history}>
// 					<Router history={history} routes={routes} store={store} />
// 				</Provider>
// 			)
//
// 			res.header("Content-Type", "text/html");
// 			res.send('<!doctype html>\n' + renderToString(<HTML content={content} store={store}/>))
// 		}
// 	})
// })

app.listen(port, function () {
	console.log('Server listening on http://localhost:%s, Ctrl+C to stop', port)
})


// import express from 'express'
// import path from 'path'
// import compression from 'compression'
// import zlib from 'zlib'
//
// const port = 3000
// const app = express()

// app.use(compression(
// 	{
// 		level: 9,
// 		memLevel: 8,
// 		strategy: zlib.Z_FILTERED
// 	}
// ))
// app.use( '/', express.static('public', {
// 	maxage: '2h',
// 	index: path.resolve(__dirname, '../../index-hot.html')
// }) )
//
//
//
// app.use(function (req, res) {
//
// 	res.sendFile( path.resolve(__dirname, '../../index-hot.html') )
//
// })
//
// app.listen(port, function () {
// 	console.log('Server listening on http://localhost:%s, Ctrl+C to stop', port)
// })