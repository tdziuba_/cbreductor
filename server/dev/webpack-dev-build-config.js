/**
 * Created by tdziuba on 2016-05-05.
 */
var webpackConfig = require('../../webpack.dev.config.js');

module.exports = function(opts) {
	var config = webpackConfig;

	if (typeof opts.context !== 'undefined' && opts.context.debug) {
		config.devtool = '...';
	}

	return config;
};