/**
 * Created by tdziuba on 2016-05-05.
 */

var build = require('webpack-build'),
	path = require('path'),
	buildCfg = path.resolve(__dirname, './webpack-dev-build-config.js');

build.workers.spawn()

build({
	config: buildCfg,
	context: {
		entry: buildCfg.entry,
		debug: true,
	},
	watch: true,
	aggregateTimeout: 200,
	poll: 1000
}, function(err, data) {
	console.log(err);
	console.log('data: ', data);
});