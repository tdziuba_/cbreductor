/**
 * Created by tdziuba on 2016-04-18.
 */
require('babel-register')({
	presets: [ "es2015", "react", "stage-0" ]
});

module.exports = require('./dev-server');