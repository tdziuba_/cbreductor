/**
 * Created by tdziuba on 01.04.2016.
 */
require('babel-register')({
	presets: [ "es2015", "react", "stage-0" ]
});

module.exports = require('./server');